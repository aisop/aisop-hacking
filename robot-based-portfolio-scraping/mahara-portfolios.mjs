import store from './download-storage.mjs';
import {setUpBrowser, loginToMahara} from './portfolio-basics.mjs';
import fs from 'fs';
import {fetchWithInclusions, fetchAlone} from './fetch-inclusions.mjs';

// you need the four following arguments (argv[0] is the node executable,
// and argv[1] is the scriptname). E.g put them in the command line
// or in the ide invocation
const username = process.argv[2],
    password = process.argv[3],
    urlList = process.argv[4],
  packName = "IT3-2022";

const useHeadlessAndPrint = false, withIncludes = true,
  timeoutBeforeLoginFormRender = useHeadlessAndPrint ? 5000: 1000;

let browser, page;



const cleanUpAuthorName = (authorName) => {
  if(!authorName) return "undefined"
  return authorName.trim().replace(/\W/, '_');
}
const cleanupTitle = (title) => {
  return title.trim().replace(/ - moopaed mahara/, "");
}

(async () => {
    let couple = await setUpBrowser(useHeadlessAndPrint, timeoutBeforeLoginFormRender)
    browser = couple[0];
    page = couple[1];
    await loginToMahara(page, browser, username, password);

    let urls = fs.readFileSync(urlList).toString().split("\n");

    for(let portfolioBase of urls) {
      if(!portfolioBase || portfolioBase.trim()==="") continue;
      let author;
      let listOfPages = [];
      try {
        await page.goto(portfolioBase);
        let currentURL = portfolioBase;

        let className;
        do {
          if (page.url().indexOf("login") !== -1) {
            loginToMahara(page);
            page.goto(currentURL);
          }
          //await page.waitForSelector("a[href*='user/view.php']");
          console.log("... " + page.url());
          const title = cleanupTitle(await page.title());
          author = await page.$eval("#pageheader-column-container a[href*='user/view.php']",
              el => el.innerText);
          const storage = new store.WebScrapingStorage("out/static",
              "out/" + cleanUpAuthorName(author));
          console.log(` ------------------- ${author} ${title} -----------------------------`)
          let filename, pdffilename = null;
          if (useHeadlessAndPrint) {
            pdffilename = await storage.createResource(page.url(),
                await page.pdf(), "application/pdf");
          }
          if (withIncludes) {
            filename = await fetchWithInclusions(page, page.url(), storage, browser);
          } else {
            filename = await fetchAlone(page, page.url(), storage, browser);
          }
          listOfPages.push({filename: filename, title: title, pdffilename: pdffilename})

          // switch page
          //await page.waitForSelector("button.nextpage")
          if (page.$("button.nextpage") == null) {
            break;
          }
          className = await page.$eval("button.nextpage", el => el.className)
          currentURL = page.url();
          if (className.indexOf('disabled') === -1) {
            console.log("Next-page");
            await Promise.all([
              page.click("button.nextpage"),
              page.waitForNavigation({waitUntil: "networkidle2"})
            ])
          }

        } while (className.indexOf('disabled') === -1)

      } catch (e) {
        console.error("Warning - portfolio " + portfolioBase + " failed.",e)
      }
      let fileList = "";
      for (let f of listOfPages) {
        fileList += `<li><a href="${escape(f.filename)}">${f.title}</a> 
         (<a href="${escape(f.pdffilename)}">pdf</a>)</li>`
      }
      fs.mkdirSync("out/" + cleanUpAuthorName(author), {recursive:true})
      fs.writeFileSync("out/" + cleanUpAuthorName(author) + "/index.html",
          `<!DOCTYPE html>
            <html><head><title>${author}</title><meta charset="utf-8"></head><body>
            <h1>${packName} : ${author}</h1>
            <ul>${fileList}
            </ul>
            <p>Gathered on ${new Date()} from <a href="${portfolioBase}">this link</a>.</p>
            </body></html>`)
    }

  console.log("Finished.")
  await browser.close();
})();
