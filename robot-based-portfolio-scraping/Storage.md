# AISOP Portfolios Storage Structure


Storage structure: the idea is that the recorded HTML is pointing to its
inclusions relatively. 

- root
  - course pack (time bound, e.g. IT3-2022)
    - static resources (hand-authored, predownloaded)
    - student name (with underscores)
      - index.html with annotations
      - Portfolios'-pages-title-and-html
      - alternative portfolio representations 
        (e.g. PDF, leap2a, save-web-page-complete as a folder)
      - extras
        - all included resources
        

HTML files should be web-viewable using this structure.
(index.html, portfolio-htmls)
The HTML files should also be annotated to denote processing states
(e.g. `data-aisop-analysis-result='xxx.html'`) and 
downloaded nature (e.g. `data-aisop-status='webscraped-v2232-on-2022-02-27-17-21'`).


