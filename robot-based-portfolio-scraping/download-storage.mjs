import path  from 'path';
import fs from 'fs';
import fsPromises from 'fs/promises'

/**
 * single-directory storing resources mapped to URLs.
 */
class Storage {

  constructor(basePath)
  {
    if(basePath.charAt(basePath.length-1)==='/') basePath = basePath.substring(0,basePath.length-1)
    this.basePath = basePath;
    if(!fs.existsSync(this.basePath) || !fs.statSync(this.basePath).isDirectory()) {
      fs.mkdirSync(this.basePath, {recursive: true});
    }
    this.listOfUrl = [];
    this.urlToFiles = {}
    this.scanDirectory();
  }

  cleanUrl(u) {
    if(!u) return;
    return u.replace(/view=[0-9]*&/, "")
  }

  scanDirectory()
  {
    const scanDirRecurse = (p)=>{
      for (let filename of fs.readdirSync(p)) {
        if(fs.statSync(path.join(p, filename)).isDirectory()) {
          scanDirRecurse(path.join(p, filename))
        } else {
          const url = this.filetourl(filename)
          this.listOfUrl.push(url)
          let q = path.join(p, filename);
          if(q.startsWith(this.basePath)) q = q.substring(this.basePath.length + 1)
          this.urlToFiles[this.cleanUrl(url)] = q;
        }
      }
    }

    scanDirRecurse(this.basePath)
  }


  filetourl(filename)
  {
    filename = filename.replace(/_\.(jpeg|png|svg|css|js|html)$/, "");
    let u = unescape(filename);
    return u;
  }

  urltofile(url, type)
  {
    let t = escape(url).replaceAll('/', '%2F');
    let l = t.toLowerCase();
    if ( type === 'text/html' && !l.endsWith('.html') && !l.endsWith('.htm'))
      t = t +"_.html";
    if ( (type === 'text/javascript' || type === 'application/javascript') && !l.endsWith('.js'))
      t = t +"_.js";
    if ( (type === 'text/css' || type === 'application/css') && !l.endsWith('.css'))
      t = t +"_.css";
    if ( (type === 'text/svg' || type === 'image/svg') && !l.endsWith('.svg'))
      t = t +"_.svg";
    if ( type === 'image/png' && !l.endsWith('.png'))
      t = t +"_.png";
    if ( type === 'image/jpeg' && !l.endsWith('.jpeg') && !l.endsWith('.jpg'))
      t = t +"_.jpeg";
    if ( type === 'application/pdf' && !l.endsWith('.pdf'))
      t = t +"_.pdf";
    return t;
  }

  hasUrl(url)
  {
    return typeof(this.urlToFiles[this.cleanUrl(url)]) !== "undefined"
  }

  getFileForUrl(url) {
    return this.urlToFiles[this.cleanUrl(url)]
  }

  async createResource(url, buffer, type)
  {
    const filename = this.urltofile(url, type);
    await fsPromises.writeFile(path.join(this.basePath, filename), buffer);
    this.listOfUrl.push(url);
    this.urlToFiles[this.cleanUrl(url)] = filename;
    return filename;
  }

}

/** A composite storage made of "background" storage (expected to be
 *  static resources), a page/portfolio storage and an "extra" storage. */
class WebScrapingStorage {
  constructor(staticPath, sessionPath)
  {
    this.backgroundStorage = new Storage(staticPath);
    this.sessionStorage = new Storage(sessionPath);
    this.extraStorage = new Storage(path.join(sessionPath, "extra"));
  }

  hasURL(url) {
    return this.backgroundStorage.hasUrl(url) || this.sessionStorage.hasUrl(url);
  }

  createResource(url, buffer, type) {
    return this.sessionStorage.createResource(url, buffer, type);
  }

  async createExtraResource(url, buffer, type) {
    return await this.extraStorage.createResource(url, buffer, type);
  }

}

export default {
  Storage
  ,
  WebScrapingStorage
};
