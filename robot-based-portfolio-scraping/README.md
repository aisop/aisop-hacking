# AISOP: Robot-browser-based Portfolio-Scraping


## Objectives

Create a pack of HTML files with all related files which can be viewed offline for each portfolio. The files should still be browsable and have a similar content and should include pictures.


## Ideas

[WebdriverIO can use a firefox profile](https://webdriver.io/docs/firefox-profile-service/)... so that cookies are there with a session for Mahara.

wget could use the firefox cookies file (at the time at least) and can reformulate links pretty well (but does not implement javascript).

WDIO: [browser.savePDF](https://webdriver.io/docs/api/browser/savePDF)
WDIO [elt.getHTML](https://webdriver.io/docs/api/element/getHTML)

Apparently a known problem: [this guy uses the robot to click](https://newbedev.com/save-complete-web-page-incl-css-images-using-python-selenium)

[Scrapy](https://scrapy.org/) is a classical in the web-scraping matter, it is a python library, however it does not use a web-browser by default, preferring simple beautifulsoup manipulations. It is an option.

This [how-to of Geshan](https://geshan.com.np/blog/2021/09/web-scraping-nodejs/) is well made and shows the alternatives in NodeJS. The code is still working well.
Puppeteer and Playwright are the robots here.

[Puppeteer](https://pptr.dev/) is a chromium packaging that has a NodeJS interface. It is a tick easier and a tick less test-oriented than the selenium family.

[Playwright](https://playwright.dev/) sounds to be a similar version but more commercial with potentially proxies to ease up the processing. The support may be quite effective if in need but the proxies may be an issue for privacy.

Funny consideration: WebScraping is undesired in some cases: [A marketer's guide to protect against webscraping](https://github.com/JonasCz/How-To-Prevent-Scraping/blob/master/README.md)


## First implementation (2022-02-24)

Following the how-to of Geshan, mahara-portfolio.js now logs-in to shibboleth, then to moopaed, then to mahara then requests the portfolio in argument and saves the html files accessible by the next button having parsed the author name.

Installation: `npm install` (this will download a puppetteer which includes a chrome).

Invoke with the command-line or IDE using the parameters given in the source; after npm install-ing of course:

- first `node iterate-all-portfolios.mjs <username> <password>`
- copy the list output at the end to `urls.txt`
- then `node mahara-portfolios.mjs <username> <password> urls.txt` (for 20 portfolios, it took about 20 minutes)

## Future Steps

- detect and fetch external websites 
  (wordpress, jimdo, wix, self-crafted...)
- question the encoding of file names that uses URL encoding 
  (that means that everytime you write to html you need to escape one more layer)
- create high-level page objects that mirror what can be read 
  on the screen (e.g. page 1, has link to next page, ...) and use them
  to check that a next page switch has happened
