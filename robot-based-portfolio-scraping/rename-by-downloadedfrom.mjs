import path from "node:path"
import fs from "node:fs"
import * as child_process from "child_process"
import Storage from "./download-storage.mjs";
const downloadStorage = new Storage.Storage("/Users/polx/projects/AISOP/aisop-hacking/robot-based-portfolio-scraping/out/downloaded")

const types={'png': 'image/png', 'jpeg': 'image/jpeg', 'jpg': 'image/jpeg', 'mp4': 'video/mp4', 'mov': 'video/quicktime'}
const basedir = process.argv[2];
for(let filename of fs.readdirSync(basedir)) {
    console.log(filename)
    const extension = filename.replace(/.*\.([^.]*)/,"$1")
    const mediatype = types[extension]
    let s = child_process.execSync('mdls -raw -name "kMDItemWhereFroms" ' +
        '"'+ path.join(basedir, filename) + '" ').toString()
    s = s.substring(7); s= s.substring(0,s.length-5)

    const newfilename = downloadStorage.urltofile(s,mediatype).toLowerCase()
    let stat;
    try { stat = fs.statSync(path.join(basedir,newfilename))} catch(e) {}
    if(!stat || !stat.isFile()) {
        child_process.execSync(`mv "${path.join(basedir, filename)}" "${path.join(basedir,newfilename)}"` )
        //fs.renameSync(path.join(basedir, filename), newfilename)
        console.log("renamed " + filename)
    } else
        fs.rmSync(path.join(basedir, filename))
}