import puppeteer from 'puppeteer';
import fs from 'fs';
import {setUpBrowser, loginToMahara} from './portfolio-basics.mjs'


const username = process.argv[2],
    password = process.argv[3],
    packName = "IT3-2022";

const useHeadlessAndPrint = false, withIncludes = false,
    timeoutBeforeLoginFormRender = useHeadlessAndPrint ? 5000: 1000;


const main = async () => {
  try {
    let l = await setUpBrowser(useHeadlessAndPrint, timeoutBeforeLoginFormRender);
    const browser = l[0], page = l[1];
    await loginToMahara(page, browser, username, password);

    await Promise.all([
      page.goto("https://www.moopaed.de/mahara/view/sharedviews.php"),
      page.waitForNavigation({waitUntil: "networkidle2"})
    ])

    let nextPageTag = null, portfolioUrls = [];
    do {
      portfolioUrls = await page.evaluate((portfolioUrls) => {
        // find all blocks of the search result
        for (let item of document.querySelectorAll(".sharedpages")) {
          if (item.querySelector(".postedon").innerText.indexOf("2022") !== -1) {
            portfolioUrls.push(item.querySelector(".title a").href)
          }
        }
        return portfolioUrls;
      }, portfolioUrls)

      console.log(portfolioUrls)
      // find next page button to click it
      nextPageTag = await page.evaluate(() => {
        let links = document.querySelectorAll(".page-link");
        for (let link of links) {
          if (link.innerText.indexOf("»") !== -1) {
            if(link.tagName)
            window.setTimeout(function() {link.click()}, 1000);
            return link.tagName.toLowerCase();
          }
        }
        return "span";
      })
      if (nextPageTag === 'a') {
        //await page.waitForNavigation({waitUntil: "networkidle2"})
        await page.waitForTimeout(5000);
      }
    } while (nextPageTag === 'a')

    console.log("Finished");
    for (let u of portfolioUrls) {
      console.log(u);
    }

    await browser.close();
  } catch (e) {
    console.log(e)
  }

};
main();



