import puppeteer from 'puppeteer';
import store from './download-storage.mjs';

const inclusionTags = ['img', 'script', 'video', 'audio', 'style', 'link', 'source'];

async function extactInclusionLinks(page){

  try {
    return await page.evaluate((inclusionTags) => {
      let tags = [];
      // find all tags that trigger an include
      for(let t of inclusionTags) {
        tags = tags.concat(Array.from(document.querySelectorAll(t)))
      }
      // find css imports
      let stylesheetPos = 0;
      for(let s of document.styleSheets) {
        for (let r of s.rules)
          if (r.constructor.name === 'CSSImportRule') {
            let a = document.createElement('a');
            a.href = r.href;
            tags.push({'import': a.href, 'styleSrc': r.href, stylesheetPos: stylesheetPos})
          }
        stylesheetPos++;
      }


      let inclusionArray = []
      let num = 0;

      tags.map((tag) => {
        let att = '';
        if(tag.src) att = 'src';
        if(tag.href) att = 'href';
        if(tag.import) att = 'import';
        const src = tag[att];
        if(!src || src.startsWith("data:")) return;
        num++;

        let inclusionInfo = {
          src, num, att
        };
        if(att==='import') {
          inclusionInfo.stylesheetPos = tag.stylesheetPos;
          inclusionInfo.styleSrc = tag.styleSrc;
        }
        inclusionArray.push(inclusionInfo)
      })

      return inclusionArray
    }, inclusionTags)

  } catch (err) {
    console.log(err)
  }
}

async function adjustInclusionLinks(page, links) {
  try {
    let adaptedLinks = await page.evaluate((links, inclusionTags) => {
      let tags = [];
      for(let t of inclusionTags) {
        tags = tags.concat(Array.from(document.querySelectorAll(t)))
      }

      for(let link of links) {
        if(!link.stylesheetPos) {
          for (let tag of tags) {
            if (tag[link.att] === link.src) {
              tag[link.att] = escape(link.newsrc)
              link.replaced = true;
            }
          }
        } else {
          let txt = document.styleSheets[link.stylesheetPos].ownerNode.innerHTML;
          link.txt = document.styleSheets[link.stylesheetPos].ownerNode.innerHTML;
          if(!txt) continue;
          txt = txt.replace(link.styleSrc, escape(link.newsrc));
          document.styleSheets[link.stylesheetPos].ownerNode.innerHTML = txt;
          link.replaced = true;
        }
      }
      return links;
    }, links, inclusionTags)
  } catch(e) {
    console.error(e);
  }
}

const fetchWithInclusions = async (win, url, storage, browser) => {

  console.log(await win.evaluate(() => {return window.title}))

  console.log("Downloading " + url);
  let inclusionArray = await extactInclusionLinks(win);
  //console.log(inclusionArray)

  for (let im of inclusionArray) {
    let filename = storage.hasURL(im.src);
    if (filename)
      im.newsrc = "../static/" + filename;
    else {
      try {
        console.log("- " + im.src);
        const response = await win.goto(im.src);
        //console.log("  response obtained.");
        if (!response.ok()) {
          console.log("Error: " + response.statusText())
          continue;
        }

        console.log(await win.evaluate(async () => {
          let reader = new FileReader()
          reader.onload = () => { console.log(reader.result)}
          let resp = await fetch("https://www.moopaed.de/mahara/artefact/file/download.php?file=420905&view=60556&view=56876&embedded=1&text=521112")
          reader.readAsDataURL(await resp.blob())
        }))

        await Promise.race([
          new Promise(async (resolve, reject) => {
            try {
              let buffer = await response.buffer();
              console.log("  buffer obtained.");
              let type = response.headers()['content-type'];
              console.log("  header obtained.");

              filename = await storage.createExtraResource(im.src,
                  buffer, type)
              //console.log("  bytes obtained.");
              im.newsrc = "extra/" + filename;
              resolve(filename);
            } catch (e) {
              console.warn("Can't record resource." + e)
              reject(e)
            }
          }),
          new Promise(x => setTimeout(x, 1 * 1000))
        ]);
        if(!im.newsrc) console.log(" - downloading " + im.src + " timed out.");
      } catch (e) {
        console.warn("Can't record resource." + e)
      }
    }
  }
  await win.goto(url)

  await win.setOfflineMode(true);
  console.log("... reformulating")
  await adjustInclusionLinks(win, inclusionArray);

  let filename = await storage.createResource(url, await win.content(), "text/html")
  await win.setOfflineMode(false);
  console.log("Downloaded " + filename);
  return filename;
}

const fetchAlone = async (win, url, storage, browser) => {
  console.log("Downloading " + url);
  let filename = await storage.createResource(url, await win.content(), "text/html")
  console.log("Downloaded " + filename);
  return filename;
}

export {fetchWithInclusions, fetchAlone}
