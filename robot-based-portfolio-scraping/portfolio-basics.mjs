import puppeteer from "puppeteer";

const setUpBrowser = async (useHeadlessAndPrint, timeoutBeforeLoginFormRender) => {
  let browser = await puppeteer.launch({headless: useHeadlessAndPrint});
  browser.timeoutBeforeLoginFormRender = timeoutBeforeLoginFormRender;
  let page = await browser.newPage();
  await page.setUserAgent(
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3419.0 Safari/537.36');
  return [browser, page]
}


const loginToMahara = async (page, browser, username, password) => {
  console.log("Logging in...")
  const navigationPromise = page.waitForNavigation();

  await page.goto('https://www.moopaed.de/');
  await navigationPromise;

  await page.evaluate(_ => {
    document.querySelector('a[href*="shibboleth"]').setAttribute('target', '_self');
  });
  await page.click('a[href*="shibboleth"]');

  await page.waitForSelector("#username")
  await page.waitForTimeout(browser.timeoutBeforeLoginFormRender);
  await page.type('#username', username);
  await page.waitForTimeout(browser.timeoutBeforeLoginFormRender);
  await page.type('#password', password);
  await page.waitForTimeout(browser.timeoutBeforeLoginFormRender);
  await page.click("button[type='submit']")
  console.log("Logged in Moopaed...")

  await page.waitForSelector("a[href*='/moodle/auth']")
  await page.click("a[href*='/moodle/auth']")
  await page.waitForSelector("a[href*='/mahara/view/share']")
  console.log("Logged in Mahara...")
}


export {setUpBrowser, loginToMahara}
