
# Description: This script checks your clipboard for changes.
# If you copy some text, the script prompts it to the terminal and stores it in an array.
# You can delete the recent text by pressing 'z'. You can quit this script by pressing CTRL+C.
# When stopping, the script automatically writes the array to a file.

# Author: Alexander Gantikow from the AISOP team; https://aisop.de/
# Credits: Thorsten Kranz and Smart Manoj; see class ClipboardWatcher

# Prerequisites:
# Run this program as admin
# - On Mac: add your invoking program (e.g. terminal) to input monitoring setting; see: https://support.apple.com/guide/mac-help/control-access-to-input-monitoring-on-mac-mchl4cedafb6/mac

# Tested on
# - macOS Monterey 12.6

import time
import datetime
import threading
import pyperclip
import re
import textwrap
import json
from pynput import keyboard
from pathlib import Path

ExportPath = "./export/" # For example: "./export/"

# Note: This class and parts of the main() block are based on code provided by Thorsten Kranz (author) and Smart Manoj (editor)
# See: https://stackoverflow.com/a/14687465
class ClipboardWatcher(threading.Thread):
    def __init__(self, filename, source, pause=5.):
        super(ClipboardWatcher, self).__init__()
        self._pause = pause         # Int, time in sec to pause listening to clipboard
        self._stopping = False      # Flag to break out from while loop in run()
        self._source = source       # The name of the source, e.g. a book name
        self.copyArray = []         # Array of objs to store process text from clipboard
        self.filename = filename    # String, the name of the filename, e.g. test.json
        self.unlockUndo = False     # Flag, enables 'undo' function to delete recent text from copyArray
        self.labeling = False       # Flag whether user is currently labeling a text
        pyperclip.copy('')          # Empty clipboard on startup

    def run(self):
        recent_value = ""
        while not self._stopping:
            tmp_value = pyperclip.paste()

            if tmp_value != recent_value:
                recent_value = tmp_value
                self.processClipboard(recent_value)
                self.unlockUndo = True
            time.sleep(self._pause)

    def stop(self):
        self.arrayToJsonFile()
        self._stopping = True

    # The function uses a regex to remove hyphenation at the end of a line
    # Examples: inspirie- ren --> inspirieren
    def removeHyphenation(self, text):
        return re.sub(r'([a-zA-Z\x7f-\xff])-\s([a-zA-Z\x7f-\xff])', r'\1\2', text)

    # Return a shortened version for console print, in case a text is super long
    def shortenText(self, text, width = 1000):
        return textwrap.shorten(text, width= width, placeholder="... (shortened)")

    def processClipboard(self, text):

        # If the user is currently labeling his/her last text, we do not process the clipboard
        if not self.labeling:

            # Clean up text from clipboard
            text = text.strip()
            text = self.removeHyphenation(text)

            if(text):
                # Print to terminal
                print ("\n------- Saved text: -------")
                print(self.shortenText(text))
                print("- Undo with Z key -")
                print("- Add label with L key -\n")

                # Build text object and append to array
                meta = {"source": self._source, "label": "unset"}
                textObj = {"text": text, "meta": meta }
                self.copyArray.append(textObj)
            else:
                print("--- Could not find text in your clipboard. Did you copy an image?")


    def reactOnKeyboardpress(self, key):
        try:
            # On keypress 'z': delete last elem in copyArray
            if (key.char == ('z') and self.unlockUndo):
                try:
                    lastCopy = self.copyArray.pop()
                    print ("\n------- Deleted text: -------")
                    print(self.shortenText(lastCopy['text'])+"\n")
                    self.unlockUndo = False # you cant delete twice
                except Exception as e:
                    print("Could not delete last entry, reason: "+str(e))
            elif (key.char == ('l')):
                self.labeling = True
                label = input("Label: ")
                lastCopy = self.copyArray[-1]
                lastCopy['meta']['label'] = label
                self.copyArray[-1] = lastCopy
                print("Added label '"+label+"'. Please continue copying!\n")
                self.labeling = False

        except AttributeError:
            # a special key was pressed, like space or shift
            pass

    def arrayToJsonFile(self):
        filename = self.filename
        if not filename.endswith('.json'): filename+=".json"
        filepath = Path(ExportPath, filename)
        counter = 1
        while filepath.is_file():
            oldFilename = filepath.name
            filepath = Path(ExportPath, filename)
            filepath = filepath.with_stem(filepath.stem + str(counter))
            print("File '"+oldFilename+"' already exists. New filename is: "+filepath.as_posix())
            counter +=1

        with open(filepath.resolve(), "w", encoding="utf-8") as f:
            f.write(json.dumps(self.copyArray))

    def isLabeling(self):
        return self.labeling

def main():

    # Enter filename used to store output file
    filename = input("\nAfter your session, a .json file with your results will be saved to '"+ExportPath+"' path. For this reason, please enter a file name (e.g. myFile.json).\nFilename: ")
    source = input("\nWhen copying from media, it is useful to remember the name of the source. For this reason, enter a source (e.g. book name).\nSource: ")

    # Init clipboard Watcher
    watcher = ClipboardWatcher(filename, source, 0.2)
    watcher.start()

    # Monitoring the keyboard with 'pynput' module
    # A keyboard listener is a threading.Thread, and all callbacks will be invoked from the thread.
    # See: https://pypi.org/project/pynput/#monitoring-the-keyboard
    pynput = keyboard.Listener(on_press=watcher.reactOnKeyboardpress)
    pynput.start()

    # While True, wait for clipboard changes
    while True:
        try:
            if not watcher.isLabeling():
                print("Waiting for changed clipboard... Press CTRL+C to stop")
                time.sleep(20)
        except (KeyboardInterrupt, SystemExit):
            watcher.stop()
            pynput.stop()
            print ("\n! Received keyboard interrupt, quitting the clipboard extractor.\nNow, a json file should be stored to '"+ExportPath+"' path.")
            break

if __name__ == "__main__":
    main()
