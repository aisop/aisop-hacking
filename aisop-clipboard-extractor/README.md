# AISOP clipboard extractor

A simple clipboard extractor which supports in copying text snippets from digital media (e.g. PDFs or websites). The .json file exported is ready to be imported by Prodigy in an NLP annotation process.

## How to use
- Clone the entire 'aisop-hacking' repository to your local machine using git clone git@gitlab.com:aisop/aisop-hacking.git
- In your terminal, move to the aisop-hacking/aisop-clipboard-extractor directory
- Install Python Version 3 if you haven't done so yet.
- Install the missing python packages ``pyperclip`` and ``pynput``
- On Mac: add your invoking program (e.g. terminal) to input monitoring setting ([see Link](https://support.apple.com/guide/mac-help/control-access-to-input-monitoring-on-mac-mchl4cedafb6/mac))
- On Windows: you might have to run your terminal as admin.
- Start the script using ``python3 aisop-clipboard-extractor.py``.
- Follow the instructions in your terminal

# Tested on
- macOS Monterey 12.6
- Microsoft Windows 10 Pro, V. 22H2
