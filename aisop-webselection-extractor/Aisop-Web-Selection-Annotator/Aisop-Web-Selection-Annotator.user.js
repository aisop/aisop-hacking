// ==UserScript==
// @name     		Aisop-Web-Selection-Annotator
// @version  		2.06
// @description Important! Follow the instructions on https://gitlab.com/aisop/aisop-hacking/-/tree/main/aisop-webselection-extractor

// @grant			none
// @require		https://code.jquery.com/jquery-3.6.1.slim.min.js
// @require		https://unpkg.com/@popperjs/core@2
// @require		https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js
// @require		https://aisop.de/rangy/rangy-core.js
// @require		https://aisop.de/rangy/rangy-selectionsaverestore.js
// @require		https://aisop.de/rangy/rangy-classapplier.js
// @require		https://aisop.de/rangy/rangy-textrange.js
// @require		https://aisop.de/rangy/rangy-highlighter.js
// @require 	https://unpkg.com/micromodal/dist/micromodal.min.js

// @match		https://mahara.aisop.de/view/*
// @match		http://mahara.aisop.de/view/*
// @match		https://en.wikipedia.org/*
// @match		https://de.wikipedia.org/wiki/*
// @match		https://dbpedia.org/page/*
// @match 	https://*.wordpress.com/*


// ==/UserScript==

// To restrict text selection to one container, change the MainID. Otherwise, leave it blank.
// Follow the instructions on https://gitlab.com/aisop/aisop-hacking/-/tree/main/aisop-webselection-extractor

const MAINID = "main";						// For e.g. https://your-maharara.com/view/* (Mahara Version 22)
//const MAINID = "mw-content-text";		// For e.g. https://en.wikipedia.org/wiki/* and https://de.wikipedia.org/wiki/*

// When you press SKOS related hotkeys on your keyboard, a skos string is added to the "SKOS output" field.
// On each keypress: do you want to add these content to you clipboard instantly? This is for your comfort :-)
const AUTOCOPYSKOS = true;

// If you create a multilingual SKOS-ontology, you want to add language details for your skos xml tags.
// By setting 'MULTILINGUAL' to true or false, you decide whether your skos xml tags will contain lang info.
const MULTILINGUAL = true;

// The annotator tries to guess the language on the website your are working.
// Here you should set the fallback language if it can't find the websites' language.
const FALLBACKLANG = "en";

// Add your name so you don't have re-type it everytime. This is for your comfort :-)
const YOURNAME = "Alexander Gantikow";


// You should not change anything below
// Unless you know what you're doing :)









// ---------- CSS INJECTION  ----------
// A function to inject css styles with greasemonkey
// Solution by: https://somethingididnotknow.wordpress.com/2013/07/01/change-page-styles-with-greasemonkeytampermonkey/
function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

// Styles for rangy
addGlobalStyle('.note {background-color: limegreen; }');
addGlobalStyle('.note:hover {cursor:pointer; }');
addGlobalStyle('[data-label] {position: relative; }');
addGlobalStyle('[data-label]:after {content: attr(data-label); padding: 2px 4px; position: absolute; left: 0; bottom: 100%; background-color: yellow; width: max-content; opacity: 0; -webkit-transition: opacity 0.1s ease-in-out; }');
addGlobalStyle('[data-label]:hover:after {opacity: 1; }');

// Styles for aisop interface
addGlobalStyle('.aisop-info-closed {z-index: 2147483647; position: fixed; left: 0; top: 0; background-color: #ccc; border: 1px solid grey; padding: 5px; border: 1px solid #ccc; font-size: 90%; cursor:pointer; display:none}');
addGlobalStyle('.aisop-info-open {z-index: 2147483647; position: fixed; left: 0; top: 0; background-color: rgba(255,255, 255,0.8); border: 1px solid grey; padding: 10px; border: 1px solid #ccc; font-size: 90%; display:none; height: 100%; overflow-y: scroll}');
addGlobalStyle('.aisop-close-info {text-align: right;	width: 100%; display: inline-block; }');
addGlobalStyle('.aisop-btn {margin-bottom: 5px; border-radius:5px; width: 100%;}');
addGlobalStyle('.aisop-btn {margin-bottom: 5px; border-radius:5px; width: 100%;}');
addGlobalStyle('.aisop-input-wrap {margin-bottom: 10px;} .aisop-input-wrap label {display:block;} .aisop-input-wrap input {display:block; width: 100%}');
addGlobalStyle('.shown {display: block !important;}');
addGlobalStyle('#nlpLabel {width: 100%;}');

// HystModal - a simple popup modal
addGlobalStyle(".hystmodal__opened {position: fixed; right: 0; left: 0; overflow: hidden; margin: auto !important;} .hystmodal__shadow{position: fixed; border:none; display: block; width: 100%; top: 0; bottom: 0; right: 0; left: 0; overflow: hidden; pointer-events: none; z-index: 5001; opacity: 0; transition: opacity 0.15s ease; background-color: black; } .hystmodal__shadow--show{pointer-events: auto; opacity: 0.6; } .hystmodal {position: fixed; top: 0; bottom: 0; right: 0; left: 0; overflow: hidden; overflow-y: auto; -webkit-overflow-scrolling: touch; opacity: 1; pointer-events: none; display: flex; flex-flow: column nowrap; justify-content: flex-start; z-index: 5002; visibility: hidden; } .hystmodal--active{opacity: 1; } .hystmodal--moved, .hystmodal--active{pointer-events: auto; visibility: visible; } .hystmodal__wrap {flex-shrink: 0; flex-grow: 0; width: 100%; min-height: 100%; margin: auto; display: flex; flex-flow: column nowrap; align-items: center; justify-content: center; } .hystmodal__window {margin: 50px 0; padding: 30px; box-sizing: border-box; flex-shrink: 0; flex-grow: 0; background: #fff; width: 600px; max-width: 100%; overflow: visible; transition: transform 0.2s ease 0s, opacity 0.2s ease 0s; transform: scale(0.9); opacity: 0; } .hystmodal--active .hystmodal__window{transform: scale(1); opacity: 1; } .hystmodal__close{position: absolute; z-index: 10; top:0; right: -40px; display: block; width: 30px; height: 30px; background-color: transparent; background-position: center center; background-repeat: no-repeat; background-size: 100% 100%; border: none; font-size: 0; cursor: pointer; outline: none; } .hystmodal__close:focus{outline: 2px dotted #afb3b9; outline-offset: 2px; } @media all and (max-width:767px){.hystmodal__close{top:10px; right: 10px; width: 24px; height: 24px; } .hystmodal__window{margin: 0; } }");

// Micromodal: a lightweight, configurable and a11y-enabled modal library written in pure JavaScript 
// See: https://micromodal.vercel.app/ and https://gist.github.com/ghosh/4f94cf497d7090359a5c9f81caf60699
addGlobalStyle(".modal {font-family: -apple-system,BlinkMacSystemFont,avenir next,avenir,helvetica neue,helvetica,ubuntu,roboto,noto,segoe ui,arial,sans-serif; } .modal__overlay {position: fixed; top: 0; left: 0; right: 0; bottom: 0; background: rgba(0,0,0,0.6); display: flex; justify-content: center; align-items: center; } .modal__container {background-color: #fff; padding: 30px; max-width: 700px; width: 500px; max-height: 100vh; border-radius: 4px; overflow-y: auto; box-sizing: border-box; } .modal__header {display: flex; justify-content: space-between; align-items: center; } .modal__title {margin-top: 0; margin-bottom: 0; font-weight: 600; font-size: 1.25rem; line-height: 1.25; color: #00449e; box-sizing: border-box; } .modal__close {background: transparent; border: 0; } .modal__header .modal__close:before { content: ' X '; } .modal__content {margin-top: 2rem; margin-bottom: 2rem; line-height: 1.5; color: rgba(0,0,0,.8); } .modal__btn {font-size: .875rem; padding-left: 1rem; padding-right: 1rem; padding-top: .5rem; padding-bottom: .5rem; background-color: #e6e6e6; color: rgba(0,0,0,.8); border-radius: .25rem; border-style: none; border-width: 0; cursor: pointer; -webkit-appearance: button; text-transform: none; overflow: visible; line-height: 1.15; margin: 0; will-change: transform; -moz-osx-font-smoothing: grayscale; -webkit-backface-visibility: hidden; backface-visibility: hidden; -webkit-transform: translateZ(0); transform: translateZ(0); transition: -webkit-transform .25s ease-out; transition: transform .25s ease-out; transition: transform .25s ease-out,-webkit-transform .25s ease-out; } .modal__btn:focus, .modal__btn:hover {-webkit-transform: scale(1.05); transform: scale(1.05); } .modal__btn-primary {background-color: #00449e; color: #fff; } @keyframes mmfadeIn {from { opacity: 0; } to { opacity: 1; } } @keyframes mmfadeOut {from { opacity: 1; } to { opacity: 0; } } @keyframes mmslideIn {from { transform: translateY(15%); } to { transform: translateY(0); } } @keyframes mmslideOut {from { transform: translateY(0); } to { transform: translateY(-10%); } } .micromodal-slide {display: none; } .micromodal-slide.is-open {display: block; } .micromodal-slide[aria-hidden='false'] .modal__overlay {animation: mmfadeIn .3s cubic-bezier(0.0, 0.0, 0.2, 1); } .micromodal-slide[aria-hidden='false'] .modal__container {animation: mmslideIn .3s cubic-bezier(0, 0, .2, 1); } .micromodal-slide[aria-hidden='true'] .modal__overlay {animation: mmfadeOut .3s cubic-bezier(0.0, 0.0, 0.2, 1); } .micromodal-slide[aria-hidden='true'] .modal__container {animation: mmslideOut .3s cubic-bezier(0, 0, .2, 1); } .micromodal-slide .modal__container, .micromodal-slide .modal__overlay {will-change: transform; }");

// Popper tooltip styles
addGlobalStyle('#tooltip-wrapper {position: relative}');
addGlobalStyle('#tooltip {background: #333; color: white; font-weight: bold; padding: 4px 8px; font-size: 13px; border-radius: 4px; display: none; z-index: 5000 }');
addGlobalStyle('#tooltip[data-show] {display: block; }');
addGlobalStyle('#arrow, #arrow::before {position: absolute; width: 8px; height: 8px; background: inherit; }');
addGlobalStyle('#arrow {visibility: hidden; }');
addGlobalStyle('#arrow::before {visibility: visible; content: ""; transform: rotate(45deg); }');
addGlobalStyle('#tooltip[data-popper-placement^="top"] > #arrow {bottom: -4px; }');
addGlobalStyle('#tooltip[data-popper-placement^="bottom"] > #arrow {top: -4px; }');
addGlobalStyle('#tooltip[data-popper-placement^="left"] > #arrow {right: -4px; }');
addGlobalStyle('#tooltip[data-popper-placement^="right"] > #arrow {left: -4px; }');


// ---------- GLOBAL VARIABLES ----------
var selAllowed = true;			// True if user is allowed to select text, false if e.g. a menu is open.
var savedSel = null;				// used to temporary save and restore a user selection in DOM
var cachedNoteText = "";   	// If user enters a note, aborts but want to continue his note;

var popper;									// Make the text-selection tooltip for popper library globally accessible
var $popper;								// Make the text-selection tooltip DOM element for jQuery globally accessible
var $popperFadeTime	= 200		// Time to fade in and fade out the text-selection tooltip
var virtualElement;					// use a virtual element instead of a DOM element to position the popper by coordinates
const { createPopper } = Popper;						// createPopper constructor
const FALLBACKID = 'aisop-annotator-active' // If user-defined container ID does not exist.

// Rangy modules
var saveRestoreModule;
var serializerModule;
var classApplierModule;

// ---------- MOBILE DEVICE DETECTION  ----------
// Code from: developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent
function hasTouchScreen() {
	var touchScreen = false;
	if ("maxTouchPoints" in navigator) {
		touchScreen = navigator.maxTouchPoints > 0;
	} else if ("msMaxTouchPoints" in navigator) {
		touchScreen = navigator.msMaxTouchPoints > 0;
	} else {
		var mQ = window.matchMedia && matchMedia("(pointer:coarse)");
		if (mQ && mQ.media === "(pointer:coarse)") {
			touchScreen = !!mQ.matches;
		} else if ('orientation' in window) {
			touchScreen = true; // deprecated, but good fallback
		} else {
			// Only as a last resort, fall back to user agent sniffing
			var UA = navigator.userAgent;
			touchScreen = (
				/\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(UA) ||
				/\b(Android|Windows Phone|iPad|iPod)\b/i.test(UA)
			);
		}
	}
	return touchScreen;
}

// ---------- SELECTOR: RANGY TEXT SELECTION ----------
// The user selected something, now modify his selection (param sel)
// Expand the range to completely encompass all words
// wordRegex: regular expr. to identify words; here: include french characters
// ignoreCharacters: Characters that should be ignored; e.g. user clicks on single !.?
// trim: Boolean specifying whether to trim trailing and leading spaces
function expandSelection(sel) {
	if (sel.isCollapsed == false) {
		sel.expand("word", {
			wordOptions: { wordRegex: /[a-zA-ZÀ-ÿ0-9]+(['\-][a-zA-ZÀ-ÿ0-9]+)*/gi },
			characterOptions: { ignoreCharacters: ",”“" },
			trim: true
		});
	}
	return sel;
}

function removeSelection(sel) {
	sel.removeAllRanges();
	hideSelectionTooltip();
}

function handleSelection(sel) {
	if (selAllowed && sel.isCollapsed == false) {
		cachedNoteText = "";
		sel = expandSelection(sel);
		showSelectionTooltip(sel);
	} else {
		//console.log("Selection is collapsed");
		hideSelectionTooltip();
	}
}

// Function checks, whether the requested highlight overlaps an existing highlight.
// If there is no overlaps, it returns false, saying that the code can continue.
// If there it is an overlap, it lets the user confirm whether to continue or not.
function handleOverlap(sel) {
  let overlap = highlighter.selectionOverlapsHighlight(sel);       
  if (!overlap) {
    return true;
  } else if (overlap && window.confirm("Warning: your selection overlaps an existing highlight. If you continue, they will be merged. Do you want to proceed?")) {
  	return true;
  } else {
  	return false;
  }
}

// ----- SELECTOR: RANGY SAVE AND RESTORE SELECTION TEMPORARY ------
// rangy.modules.SaveRestore module saves and restores a user selection
// It uses invisible marker elements in the DOM
// E.g. as soon the user open a model, the focus changes. We are now able to restore t. selection
function saveTemporarySelection() {
	if (rangy.supported && saveRestoreModule && saveRestoreModule.supported) {
		savedSel = rangy.saveSelection();
	}
}

function restoreTemporarySelection() {
	if (savedSel) {
		rangy.restoreSelection(savedSel, true);
		savedSel = null;
	}
}

// ---------- POPPER SELECTION-TOOLTIP ----------
// A function helping to position the popper tooltip via absolute coordinates
function generateGetBoundingClientRect(x = 0, y = 0) {
  return () => ({ width: 0, height: 0, top: y, right: x, bottom: y, left: x,});
}

// We use "popper" library to show a tooltip below the text-selection
function showSelectionTooltip(sel) {
	if (sel.isCollapsed == false) {
    saveTemporarySelection();
    // Print word and char count to tooltip
    let rawText = sel.toString();
    let wordCount = countWords(rawText);
    $('#countWords').text(wordCount);
    $('#countChars').text(rawText.length);
    // Position tooltip below selection    
    var range = sel.getRangeAt(0);
    range.collapse(false);
    range.move("character", 1);
    sel.setSingleRange(range);
    boundingClient = rangy.getNativeSelection().getRangeAt(0).getBoundingClientRect();    
		virtualElement.getBoundingClientRect = generateGetBoundingClientRect(boundingClient.x-10, boundingClient.y+25);
    popper.update();
    restoreTemporarySelection();
    $($popper).attr('data-show', '');
	}
}

function hideSelectionTooltip() {
  $($popper).removeAttr('data-show'); 
}

// ---------- SELECTOR: TEXT HELPER ----------

// Counts the words in a given string.
// It is assmumed that words are seperated by spaces
function countWords(str) {
	counter = 0;
	var words = str.split(' ');
	$.each(words, function (i, word) {
		if (word != "" && word != " ") {
			counter += 1;
		}
	});
	return counter;
}


// Change attribute in classaplier and by this an attribute in the highlight itself
// Param hl: highlight object, containing all classappliers (e.g  for yellow, blue, red...)
// Param name: the ClassApplier name to modify a specific classapplier (e.g. yellow, text or happy)
// Param data: dataKey (string) to know which data-attribute, e.g. "data-sharedid" 
// Param dataVal: to change with what value
function setClassApplierDataAttribute(hl, name, dataKey, dataVal) {
	return hl.classAppliers[name].elementAttributes[dataKey] = dataVal;
}

function rangy_removeAllHighlights() {
  highlighter.removeAllHighlights();
  sessionStorage.removeItem('rangy');
}

// Function returns the pathname of the current browser url/location.
// For https://en.wikipedia.org/wiki/Array_(data_structure)# ...
// it returns: wiki-Array-data-structure
// We use the result to e.g. build a filename for json export
function getPathname() {
  let pathname = $(location).attr('href');
  pathname = pathname.replace("https://", "");
	pathname = pathname.replace("http://", "");
  pathname = pathname.replace(/[\W_]/g, "-");
  pathname = pathname.replace(/^-+|-+$/g, '');
  pathname = pathname.replace(/\-+/g, '-');
  if (pathname.length == 0) {pathname = "unknown.json";}
  else {pathname += ".json";}
  return pathname;
}

// ----------------------------------------------------------------------------------------------------------------------
// ---------------------------------------- EXPORT ----------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------

// Wraps a given highlights' text to a skos tag.
// Param 'tag' is a skos tag, e.g. "skos:prefLabel".
function textToSkos(text, tag, lang="") {
  if(lang.length > 0) {
    var skos = `<${tag} lang="${lang}">${text}</${tag}>\n`;
  } else {
    var skos = `<${tag}>${text}</${tag}>\n`;
  }
  return skos;
}

function handleSkosMatch(matchType) {
  let definedBy = $('#isDefinedBy').val();
  var skos = textToSkos(definedBy, matchType);
  return skos;
}

// Function adds a given text (e.g. skos) to a textarea (e.g. skosOutput)
function addTextToOutput(text, id="skosOutput") {
  if(text.length > 0) {
    var val = $('#'+id).val();
    if(val.includes(text)) {return;}    
    var newVal = val !== undefined || val.length > 0 ? val + "\n" + text : text;

    let addUrl = $('#addUrl').is(':checked');
    if(addUrl) {
      url = $(location).attr('href');
      url = textToSkos(url, "rdfs:seeAlso");
      if (!newVal.includes(url)) {newVal += "\n"+url}
    }

    $('#'+id).val(newVal);
    if(AUTOCOPYSKOS) {inputToClipboard(id)}
	} else {
    console.error("No text given to add to Output field")
  }
}

// Get the text field, select it and copy its contents.
// Solution by: https://www.geeksforgeeks.org/how-to-copy-the-text-to-the-clipboard-in-javascript/ 
function inputToClipboard(id) {
    try {   
      let inputField = document.getElementById(id);
      inputField.select();
      document.execCommand("copy");
      console.log("Copied the following content:\n"+ inputField.value);
    } catch (error) {
        console.error('Failed to copy to clipboard with error: ' + error);
    }
}

function getLanguage() {
	return $('#skosLang').val();
}

// Prints all higlights to console to be inserted in json document
// Note: You have to remove the "line" info when you copy from the console
function highlightsToJsonString(meta, type="text") {
  let arr = highlights_to_array(meta, type);
  return JSON.stringify(arr);
}

// Converts all rangy-highlights to one array in a readable format (text and/or html)
function highlights_to_array(meta, type) {
  let highlightArray = [];
  highlighter.highlights.forEach(function(h,i){
    var newMeta = structuredClone(meta);
    newMeta['document'] = meta.userid+"_"+meta.title+"_"+i;
    newMeta['label'] = h.label;

    if (type == "text") {
      var obj = {"text":h.getText(), "meta": newMeta};
      highlightArray.push(obj);
    } else if (type == "html") {
      var range = h.getRange();
      range.select();
      var sel = rangy.getSelection();
      var obj = {"text": h.getText(), "html":sel.toHtml(), "meta": newMeta};
      highlightArray.push(obj);
    }
  });
  //console.log(highlightArray);
  return highlightArray;
}

// Build a file and make it downloadable
// Source: https://jsfiddle.net/Stelios2020/ukmf5304/6/
function downloadfile(content, mimeType, filename){
  var a = document.createElement('a')
  var blob = new Blob([content], {type: mimeType})
  var url = URL.createObjectURL(blob)
  a.setAttribute('href', url)
  a.setAttribute('download', filename)
  a.click()
}


// ----------------------------------------------------------------------------------------------------------------------
// ---------------------------------------- DOCUMENT READY --------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------

$( document ).ready(function() {
  
  // ------ FALLBACK TO ID ------
  // If the annotator can't find the user-defined constant 'MAINID' it adds a fallback-ID to 'body'
  if (MAINID.length == 0 || $('#'+MAINID).length == 0){
  	console.error('The user defined MAINID is either empty or does not exist on this website. Falling back to "FALLBACKID"');
  	$('body').prop('id', FALLBACKID);
  	mainId = FALLBACKID;
  }
  else {
    mainId = MAINID;
  }
   
	// ------ ANNOTATOR: INITIALIZE RANGY MODULES ------
	// saveRestoreModule saves and restores a user selection
	// serializerModule serializes and deserializes ranges and selections
	// classApplierModule adds, removes and toggles classes on text nodes
	// TODO: Check here whether Rangy modules were able to initialize
	saveRestoreModule = rangy.modules.SaveRestore;
	serializerModule = rangy.modules.Serializer;
	classApplierModule = rangy.modules.ClassApplier;
  
  
  // ------ APPEND TO DOM ------
  // Annotator Info: Add info element on top left of the screen
  // AddTooltip: tooltip below selection, showing count of words and chars
  // AddModal: modal containing a form, allowing to add notes/ labels to a highlight
  addAnnotatorInfo();
  addTooltip();  
  addModal();
  
  // Init MicroModal
  MicroModal.init();
  
  // ------ FILL FORM FIELDs ------
  // For some SKOS definitions its important to set the language like @en
  if(MULTILINGUAL) {
    htmlLang = $('html').attr('lang');
    if(htmlLang === undefined) {
      $('#skosLang').val(FALLBACKLANG)
    } else {
      $('#skosLang').val(htmlLang)
    }
  }

  
  // Update the URL and filename in website formular
  // Set default name in forms
  let href = $(location).attr('href');
  $('#urlWebsite').val(href);
  $('#filenameWebsite').val(getPathname());  
  if (YOURNAME.length > 0) {$('.authorName').val(YOURNAME);}
  
	// In case ob dbpedia, we get a URI in describedby property.
  let describedby = $('link[rev="describedby"').attr('href');
  if(describedby) {
    $('#isDefinedBy').val(describedby);
  } else {
    $('#isDefinedBy').val(href);
  }
  
	// Create Popper instance with custom options. 
  // virtualElement: DomRect Element made of coordinates
  // $popper: The HTML / XML element used as the tooltip
  // Modifier: Disable listeners so tooltip stays at selected text when scrolling
  virtualElement = { getBoundingClientRect: generateGetBoundingClientRect() };
  $popper = document.querySelector('#tooltip'); 
  popper = createPopper(virtualElement, $popper, {
		placement: "bottom",    
    offset: { offset: "0,5" },
    modifiers: [{ name: 'eventListeners', enabled: false }],    
	}); 
    
	// ------ SELECTOR: INITIALIZE RANGY------
	// Rangy initializes itself but .init() ensures that its ready to use
  rangy.init();

  // ------ INITIALIZE RANGY HIGHLIGHTER ------
  // Create a highlighter object for the DOM 'Document' object.
  // The type parameter uses 'TextRange' instead of 'textContent' (default) because
  // highlights will more likely work cross-browser. But it is very slow on large DOM.
  // For details see: https://github.com/timdown/rangy/wiki/Highlighter-Module
  highlighter = rangy.createHighlighter(document);
  
  highlighter.addClassApplier(rangy.createClassApplier("note", {
    elementAttributes: {},
    // If a highlight is created, this event fires:
    onElementCreate: function (el, classapp) {
      var label = classapp.elementAttributes['data-label'];
      if(label !== undefined && label.length == 0) {$(el).removeAttr("data-label");}
    },
    elementProperties: {
      href: "#",
      onclick: function() {
        var highlight = highlighter.getHighlightForElement(this);
        if (window.confirm("Delete this highlight (ID " + highlight.id + ")?")) {
          highlighter.removeHighlights( [highlight] );
          sessionStorage.setItem("rangy", highlighter.serialize());
        }
        return false;
      }
    }
  }));


	// ------ SELECTOR: EVENT LISTENERS ------
	// If the user has (no) touchscreen initialize events differently
	if (hasTouchScreen()) {
		// While the user selects a text, selectionchange would fire with each letter.
		// We prevent this adding a timeout before the the selection is processed.
		var timeout;
		document.addEventListener("selectionchange", function (e) {
			clearTimeout(timeout);
			var sel = rangy.getSelection();
			timeout = setTimeout(showSelectionTooltip(sel), 200);
		}, false);
	} else {
    // If user selects some text, selection handling starts with handleSelection() function
		$('#'+mainId).on('mouseup', function() {handleSelection(rangy.getSelection());});
		$(".selectable-text").on('mouseup', function() {handleSelection(rangy.getSelection());});
		$("html").on('mousedown', function() {hideSelectionTooltip();});
	}  

  // Restore highlights from sessionStorage
  serializedHighlights = sessionStorage.getItem("rangy");
  if (serializedHighlights) {
    highlighter.deserialize(serializedHighlights);
  }
  
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------------------------------- SKOS --------------------------------------------------------------------
  // ----------------------------------------------------------------------------------------------------------------------

  // Method empties the #skosOutput input-field
  $('#emptySkos').on( "click", function() {
    $('#skosOutput').val("");
  });
  
  // Handler copies the #skosOutput to clipboard
  $('#copySkos').on( "click", function() { inputToClipboard('skosOutput') });
    
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------------------------------- KEYPRESS --------------------------------------------------------------------
  // ----------------------------------------------------------------------------------------------------------------------

  // On keypress 'h': Create highlight and overwrite session storage
  // On keypress 'l': Open a modal to add a label to the highlight which is created afterwards
  // On keypress 'p': Expand selection to its element
  // On keypress 'd': Add a skos:definition to skos output field
  // On keypress 'a': Add a skos:altLabel to skos output field
  
  
  prevNode = null;
  document.addEventListener("keypress", function(event) {
    let sel = rangy.getSelection();
    if (sel.isCollapsed == false) {
      
      // -------------------- H --------------------
      if (event.key === "h") {
        hideSelectionTooltip();
        let proceed = handleOverlap(sel);
				if (proceed) {
          let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
          sessionStorage.setItem("rangy", highlighter.serialize());
          sel.removeAllRanges(); 
        }       
      } 
      
      // -------------------- S --------------------
      else if(event.key === "s") {        
        let node = sel.anchorNode.parentNode;
        var range = rangy.createRange();
        range.selectNodeContents(node);
        range.select();
      } 
      
      // -------------------- L --------------------
      else if(event.key === "l") {
        let proceed = handleOverlap(sel);
				if (proceed) {
          hideSelectionTooltip();
          saveTemporarySelection();
          MicroModal.show('note-modal');
        }
      }

      // -------------------- D --------------------
      if (event.key === "d") {
        hideSelectionTooltip();
        let proceed = handleOverlap(sel);
				if (proceed) {
          let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
          let lang = getLanguage();
          let text = hl[0].getText()
          let skos = textToSkos(text, "skos:definition", lang);
          addTextToOutput(skos);
          sessionStorage.setItem("rangy", highlighter.serialize());
          sel.removeAllRanges(); 
        }       
      } 
          
      // -------------------- E --------------------
      if (event.key === "e") {
        hideSelectionTooltip();
        let proceed = handleOverlap(sel);
				if (proceed) {
          let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
          let lang = getLanguage();
          let text = hl[0].getText()
          let skos = textToSkos(text, "skos:example", lang);
          addTextToOutput(skos);
          sessionStorage.setItem("rangy", highlighter.serialize());
          sel.removeAllRanges(); 
        }       
      }
      
      // -------------------- P --------------------
      if (event.key === "p") {
        hideSelectionTooltip();
        let proceed = handleOverlap(sel);
				if (proceed) {
          let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
          let lang = getLanguage();
          let text = hl[0].getText()
          let skos = textToSkos(text, "skos:prefLabel", lang);
          addTextToOutput(skos);
          sessionStorage.setItem("rangy", highlighter.serialize());
          sel.removeAllRanges(); 
        }       
      }
      
      // -------------------- N --------------------
      if (event.key === "n") {
        hideSelectionTooltip();
        let proceed = handleOverlap(sel);
				if (proceed) {
          let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
          let lang = getLanguage();
          let text = hl[0].getText()
          let skos = textToSkos(text, "skos:note", lang);
          addTextToOutput(skos);
          sessionStorage.setItem("rangy", highlighter.serialize());
          sel.removeAllRanges(); 
        }       
      }
      
      // -------------------- A --------------------
      if (event.key === "a") {
        hideSelectionTooltip();
        let proceed = handleOverlap(sel);
				if (proceed) {
          let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
          let lang = getLanguage();
          let text = hl[0].getText()
          let skos = textToSkos(text, "skos:altLabel", lang);
          addTextToOutput(skos);
          sessionStorage.setItem("rangy", highlighter.serialize());
          sel.removeAllRanges(); 
        }       
      }
           
      // -------------------- O --------------------
      // This is rather buggy.
      else if(event.key === "o") {
        let anchorNode = sel.anchorNode;
        let previousSibling = anchorNode.previousSibling;
        if (previousSibling != null) {
          previousSibling = previousSibling.previousSibling;
        }
        let newNode = anchorNode;
        while (previousSibling == null) {
          newNode = newNode.parentNode;
          previousSibling = newNode.previousSibling.previousSibling;
        }
        var previousRange = rangy.createRange();
        previousRange.selectNodeContents(previousSibling);
        sel = rangy.getSelection();
        sel.addRange(previousRange);
      }      

    }
    else {
      //console.log("isCollapsed")
      prevNode = null;
    }
    
    // End of text-selection-based keypresses
    // Below are keypresses possible without text selection
    
    // -------------------- 1 --------------------
    if (event.key === "1") {
				skos = handleSkosMatch("skos:closeMatch");
        addTextToOutput(skos);    
    }
    else if (event.key === "2") {
				skos = handleSkosMatch("skos:exactMatch");
        addTextToOutput(skos);    
    }
    else if (event.key === "3") {
				skos = handleSkosMatch("skos:broadMatch");
        addTextToOutput(skos);    
    }
    else if (event.key === "4") {
				skos = handleSkosMatch("skos:narrowMatch");
        addTextToOutput(skos);    
    }
    else if (event.key === "5") {
				skos = handleSkosMatch("skos:relatedMatch");
        addTextToOutput(skos);    
    }
    
    
  });
  


  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------------------------------- AISOP ANNOTATOR INTERFACE ---------------------------------------------------
  // ----------------------------------------------------------------------------------------------------------------------

  
  // Open Aisop Annotator Interface
  $(".aisop-info-closed" ).click(function() {
			$(this).removeClass("shown");
    	$(".aisop-info-open").addClass("shown");
	});
  
  // Close Aisop Annotator Interface
  $( ".aisop-close-info").click(function() {
			$(".aisop-info-open").removeClass("shown");
    	$(".aisop-info-closed").addClass("shown");
	});
  
  // Trash button in info element to delete all highlights
  $( "#trash_btn" ).click(function() {
    if (confirm("Are you sure?") == true) {
      rangy_removeAllHighlights();
    }
	});
  
  // Serialize button in info element
  $( "#serialize_btn" ).click(function() {
  	let serialized = highlighter.serialize();
    $('#serialize_result').val(serialized);
	});
  
  // Deserialize button in info element (= restore highlights)
  $( "#deserialize_btn" ).click(function() {
    if (confirm("Are you sure? This will replace all existing highlights!") == true) {
      rangy_removeAllHighlights();
      let content = $('#deserialize_content').val();
      highlighter.deserialize(content);
      sessionStorage.setItem("rangy", highlighter.serialize());
    }
	});
  
  // User can choose between multiple <form>
  $('#type').change(function() {
    $('.aisop-form').hide();
    let val = $(this).val();
    $('#'+val).show();
  });
  
  // Set URL field to current url
  var url = document.location.href; 
  $('#url').val(url);
  
  // Submit: to_json button in "#portfolio" <form>
  // If user exports his/her highlights in an E-portfolio to json.
  $('#portfolio').validate({
    rules: {
      author: {required: true},
      title: {required: true },      
      userid: {required: true, minlength: 4 },
      provider: {required: true },
      url: {required: true },
      export_type: {required: true }
    },
    submitHandler: function (form, e) {
      let meta = {
        type: "portfolio",
        author: $('#authorNamePortfolio').val(),
      	title: $('#portfolioTitle').val(),
        userid: $('#portfolioUserId').val(),
        provider: $('#portfolioProvider').val(),
        date: new Date().toLocaleDateString()
      };
      console.log(meta);
      let export_type = $('#portfolioExport').val();
      
      // Export to json or html
      let format = $(this.submitButton).data('format');
      if(format == 'json') {      
      	let jsonString = highlightsToJsonString(meta, export_type);
        let fileName = meta.userid + '_' + meta.title + '.json';
      	downloadfile(jsonString, 'application/json', fileName);
			} else if(format == 'html') {
        let htmlString = new XMLSerializer().serializeToString(document);
        let fileName = meta.userid + '_' + meta.title + '.html';
        downloadfile(htmlString, "text/html", fileName)
      }
      
      return false;
    }
  });
  
  // Submit: to_json button in "#website" <form>
  // If user exports his/her highlights in an website to json.
  $('#website').validate({
    rules: {
      author: {required: true},
      url: {required: true }
    },
    submitHandler: function (form, e) {
      let fileName = $('#filenameWebsite').val();
      if (!fileName.endsWith(".json")) {fileName+= ".json";}
      
      let meta = {
        type: "website",
        author: $('#authorNameWebsite').val(),
      	url: $('#urlWebsite').val(),
        fileName: fileName,
        date: new Date().toLocaleDateString()
      };
      console.log(meta);
      let export_type = $('#websiteExport').val();
      
      // Export to json or html
      let format = $(this.submitButton).data('format');
      if(format == 'json') {      
      	let jsonString = highlightsToJsonString(meta, export_type);
      	downloadfile(jsonString, 'application/json', fileName);
			} else if(format == 'html') {
        let htmlString = new XMLSerializer().serializeToString(document);
        downloadfile(htmlString, "text/html", fileName)
      }
      
      return false;
    }
  });
  
  // User submits note/label in modal
  $('#note-form').validate({
    rules: {
      nlpLabel: {required: true },
    },
    submitHandler: function (form) {
      // Close modal, restore selection & hide tooltip
      MicroModal.close('note-modal');
      restoreTemporarySelection();
      hideSelectionTooltip();
      // Create highlight with data-label      
      let label = $('#nlpLabel').val();
      setClassApplierDataAttribute(highlighter, "note", "data-label", label);
      let hl = highlighter.highlightSelection("note", { containerElementId: mainId });
      hl = highlighter.setHighlightsAttribute(hl, "label", label);
      console.log("Created this highlight:");
      console.log(hl);
      sessionStorage.setItem("rangy", highlighter.serialize());
      // Add label to array for autocomplete
      // Get selection to remove it from browser
      var sel = rangy.getSelection();
      sel.removeAllRanges();
      // Reset label in classapplier (onelementcreate was fired by this time)
      setClassApplierDataAttribute(highlighter, "note", "data-label", "");
      // Avoid page refresh
      return false;
      
    }
  });
  
  
}); // end of 'dom ready'

// ----------------------------------------------------------------------------------------------------------------------
// ---------------------------------------- DOM APPENDINGS --------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------

function addAnnotatorInfo() {
	
	// Aisop info interface "closed"
  $('body').prepend(`
		<div class="aisop-info-closed shown">
			Open Aisop Annotator
		</div>
	`);
  
  // Aisop info interface "opened"
  $('body').prepend(`
		<div class="aisop-info-open">
			
			<a href="#" class="aisop-close-info">close</a>
			<h4>AISOP Annotator</h4>
			
			<!---------------- SOURCE PICKER ---------------->
			<form>
					<div class="aisop-input-wrap">
            <label for="type">Source</label>
            <select name="type" id="type">
              <option value="portfolio" selected="selected">Portfolio</option>
              <option value="website">Website</option>
              <option value="openbook">Open Book</option>
            </select>
					</div>
			</form>

			<!---------------- PORTFOLIO FORM ---------------->
			<form id="portfolio" class="aisop-form">

				<div class="aisop-input-wrap">
          <label for="authorNamePortfolio">Your Name</label>
          <input type="text" id="authorNamePortfolio" class="authorName" name="authorNamePortfolio" required minlength="1" maxlength="40" >
				</div>
				
				<div class="aisop-input-wrap">
          <label for="portfolioTitle">Portfolio-Title</label>
          <select name="portfolioTitle" id="portfolioTitle">
            <option value="oop">oop</option>
            <option value="oop_schwerpunkt">oop_schwerpunkt</option>
            <option value="oop_uebung">oop_uebung</option>
            <option value="fundideen">fundideen</option>
            <option value="fundideen_schwerpunkt">fundideen_schwerpunkt</option>
            <option value="fundideen_uebung">fundideen_uebung</option>
            <option value="grundlagen_informationstechnologie">grundlagen_informationstechnologie</option>
            <option value="grundlagen_informationstechnologie_schwerpunkt">grundlagen_informationstechnologie_schwerpunkt</option>
            <option value="llcm">llcm</option>
            <option value="llcm_schwerpunkt">llcm_schwerpunkt</option>
            <option value="html">html</option>
            <option value="html_schwerpunkt">html_schwerpunkt</option>
            <option value="informationssysteme">informationssysteme</option>
            <option value="informationssysteme_schwerpunkt">informationssysteme_schwerpunkt</option>
            <option value="audiovis_medien">audiovis_medien</option>
            <option value="audiovis_medien_schwerpunkt">audiovis_medien_schwerpunkt</option>
          </select>
				</div>

				<div class="aisop-input-wrap">
          <label for="portfolioUserId">User-ID</label>
          <input type="text" id="portfolioUserId" name="portfolioUserId" required minlength="4" >
				</div>

				<div class="aisop-input-wrap">
          <label for="portfolioProvider">Provider</label>
          <select name="portfolioProvider" id="portfolioProvider">
            <option value="mahara">mahara</option>
            <option value="wordpress">wordpress</option>
          </select>
				</div>
				
				<div class="aisop-input-wrap">
          <label for="portfolioExport">Export-Settings</label>
          <select name="portfolioExport" id="portfolioExport">
            <option value="text">Only text</option>
            <option value="html">Text and Html</option>
          </select>
				</div>

				<div class="aisop-input-wrap">
					<button class="aisop-btn" type="submit" data-format="json">Start Json Export</button>
					<button class="aisop-btn" type="submit" data-format="html">Start HTML Export</button>
				</div>

			</form>

			<!---------------- WEBSITE FORM ---------------->
			<form id="website" class="aisop-form" style="display:none;">

        <div class="aisop-input-wrap">
          <label for="authorNameWebsite">Your Name</label>
          <input type="text" id="authorNameWebsite" class="authorName" name="authorNameWebsite" required minlength="1" maxlength="40" >
        </div>

        <div class="aisop-input-wrap">
          <label for="urlWebsite">Source URL:</label>
          <input type="url" name="urlWebsite" id="urlWebsite" placeholder="https://example.com" required />
        </div>

        <div class="aisop-input-wrap">
          <label for="isDefinedBy">rdfs:definedBy</label>
          <input type="url" name="isDefinedBy" id="isDefinedBy" placeholder="http://dbpedia.org/resource" required />
        </div>

        <div class="aisop-input-wrap">
          <label for="filenameWebsite">Filename</label>
          <input type="text" id="filenameWebsite" name="filenameWebsite" required minlength="1">
        </div>

        <div class="aisop-input-wrap">
          <label for="websiteExport">Export-Settings</label>
          <select name="websiteExport" id="websiteExport">
          <option value="text">Only text</option>
          <option value="html">Text and Html</option>
        </select>
        </div>

        <div class="aisop-input-wrap">
          <button class="aisop-btn" type="submit" data-format="json">Start Json Export</button>
          <button class="aisop-btn" type="submit" data-format="html">Start HTML Export</button>
        </div>

			</form>
			<hr>

			<!---------------- SKOS OUTPUT ---------------->
			<h6>SKOS output</h6>
			<form>
				<div style="margin: 10px 0;">
          <input type="checkbox" id="addUrl" name="addUrl" checked />
          <label for="addUrl">Add URL (rdfs:seeAlso)</label>
				</div>
				<div class="aisop-input-wrap">
					<label for="skosLang">Language:</label>
					<input type="text" id="skosLang" name="skosLang" />
				</div>
				<div class="aisop-input-wrap">
          <label style="visibility: hidden; height:0;" for="skosOutput">SKOS output</label>
          <textarea id="skosOutput" name="skosOutput" rows="5" style="width: 100%"></textarea>
        </div>
				<div class="aisop-input-wrap">
					<button type="button" id="emptySkos" class="aisop-btn" style="width:49%">Empty</button>
					<button type="button" id="copySkos" class="aisop-btn" style="width:49%">Copy</button>
				</div>
			</form>


			<!---------------- SAVE/RESTORE ---------------->
			
			<h6>Save & Restore</h6>
      <button id="serialize_btn" class="aisop-btn">Serialize highlights</button>
      <input type="text"  id="serialize_result" name="serialize_result">
			<hr>
      <button id="deserialize_btn" class="aisop-btn">Restore highlights</button>
      <input type="text"  id="deserialize_content" name="deserialize_btn">
			<hr>
      <button id="trash_btn" class="aisop-btn">Delete all highlights</button>
			
		</div>
	`);
}

function addTooltip() {
	$('body').prepend(`
		<div id="tooltip-wrapper">
      <div id="tooltip" role="tooltip">
        <span>Words: </span>
        <span id="countWords"></span>
        <span> / Chars: </span>
        <span id="countChars"></span>
        <div id="arrow" data-popper-arrow></div>
      </div>
		</div>
	`);
}


// Adds a modal to DOM
// https://micromodal.vercel.app/
function addModal() {
	$('body').append(`
  <div class="modal micromodal-slide" id="note-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
      <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="note-modal-title">
        <header class="modal__header">
          <h2 class="modal__title" id="note-modal-title">
            Add a label
          </h2>
          <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
        </header>
				<form id="note-form" class="aisop-form">

          <main class="modal__content" id="note-modal-content">
						<div class="aisop-input-wrap">
              <label for="nlpLabel">Label</label>
              <input type="text" id="nlpLabel" name="nlpLabel" required>
						</div>
          </main>

          <footer class="modal__footer">
            <button class="modal__btn modal__btn-primary" type="submit">Save</button>
            <button class="modal__btn" data-micromodal-close aria-label="Close this dialog window">Abort</button>
          </footer>

				</form>

      </div>
    </div>
  </div>
	`);
}