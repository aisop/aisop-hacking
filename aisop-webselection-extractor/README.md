# aisop-webselection-extractor 

A Greasemonkey-Script to higlight and copy text from E-portfolios and websites in your browser to create a nlp training dataset ready to be processed by [prodigy](https://prodi.gy/). The script also allows to extract information on the web as XML, inspired by [SKOS](https://www.w3.org/2009/08/skos-reference/skos.html). 

## Installation

Please install the Greasemonkey plugin in your Firefox browser. The 'monkey' icon of Greasemonkey should now appear in your browsers' plugin section. Now, click on the icon and import the .zip file of this repository with your Greasemonkey. Afterwards, the line 'Aisop-Web-Selection-Annotator' should appear in your Greasmonkey's list of available scripts. Alternatively, you can also create yourself a new Greasemonkey scipt and copy the entire script from the files `Aisop-Web-Selection-Annotator/Aisop-Web-Selection-Annotator.user.js` to your newly created script. 

## Configuration

In order to use the 'Aisop-Web-Selection-Annotator' you have to configure it to your needs. Please open Greasemonkey via your browsers' plugin section and edit the 'Aisop-Web-Selection-Annotator' script. It is written in JavaScript.

- Match: Please adjust the @match lines (about in line 16 and 17) and replace the URL by your portfolio of website location. The @match line indicates under which url the script should become active. Some examples were added to the script.
- Main ID: For text selection the 'rangy.js' library is used. Rangy allows to limit the text selection to a specific area (container) of your website. If you are familiar with your browsers 'Tools for developers', you can easily find out a suitable container ID. If you are not familiar with these tools, you might leave this variable empty like `const MAINID = ""`. However, not setting this ID might lead to problems when restoring your highlights from session storage. Some examples were added to the script.
- Autocopy SKOS: If you take a look at the 'Hotkeys' section, you will see that you can highlight text and generate SKOS output. This output is appended to the field of the same name in your annotator sidebar. If you want to process this content further, you probably want to copy it to your clipboard. With `const AUTOCOPYSKOS = true;` the 'Aisop-Web-Selection-Annotator' will do it automatically for you if you press a SKOS-Hotkey.
- Multilingual: If you create a multilingual SKOS-ontology, you want to add language details for your skos xml tags. By setting 'MULTILINGUAL' to true or false, you decide whether your skos xml tags will contain lang info. 
- Fallback language: The annotator tries to guess the language on the website your are working. Here you should set the fallback language if it can't find the websites' language.
- Your Name: To be able to understand who has created a dataset, you might to enter your name. You can enter your name every time you extract a new page in the graphical interface or you can set your name as default in the script.

## Start your selection
Now, when opening an URL matching the @match line in your browser, the button "Open Aisop Annotator" should appear in the top left corner. After pressing, a sidebar should appear. Before text extraction, you should fill in some details which will be added as metadata to your resulting file in the end. Now, start and select some text. A little tooltip should appear below or above your selection. Go on and press one of the following hotkeys.

## Hotkeys

When selection a letter or an incomplete word, the extractor expands the selection to the whole word. Afterwords you might press the following Hotkeys to manipulate and highlight your selection:

- S: Expand your selection to holistic **section** (paragraph)
- H: Simply **highlight** the selection
- L: Highlight the selection and add a **label** to it. When exporting your highlights, the script will add this label as metadata to the text. This meta information is presented in prodigy below an annotation prompt and might help your annotator in decision making.

For deletion, simply click on a highlight and confirm the prompt.

The annotator also provides hotkeys to markup your selections in XML. In the AISOP project, we model [SKOS ontologies(https://www.w3.org/TR/skos-reference/)] with the graphical software '[Cmap Tools](https://cmap.ihmc.us/)' by IHMC. Because the editor only provides one text field and thus has limited functionality to describe concepts in detail, we subdivide our text extractions with XML tags. We also use a converter that converts the ontology to SKOS. By this, the XML information can be parsed and to SKOS. The following hotkeys helps you to create such XML tags. With each of these hotkeys, the "SKOS output" field gets updated in the annotator sidebar. You can copy its content into Cmap in order to describe your concept.

- D: Wraps the selected text in a 'skos:definition' xml-tag.
- E: Wraps the selected text in a 'skos:example' xml-tag.
- P: Wraps the selected text in a 'skos:prefLabel' xml-tag.
- A: Wraps the selected text in a 'skos:altLabel' xml-tag.
- N: Wraps the selected text in a 'skos:note' xml-tag.
- 1: Wraps the rdfs:definedBy input field as 'skos:closeMatch' xml-tag.
- 2: Wraps the rdfs:definedBy input field as 'skos:exactMatch' xml-tag.
- 3: Wraps the rdfs:definedBy input field as 'skos:broadMatch' xml-tag.
- 4: Wraps the rdfs:definedBy input field as 'skos:narrowMatch' xml-tag.
- 5: Wraps the rdfs:definedBy input field as 'skos:relatedMatch' xml-tag.

Depending on your setting of the "Multilingual" variable, the annotator will add the language details to the xml-tags.

## Export your highlights

The 'Aisop Web Selection Annotator' provides three ways to save and export generated highlights:

- .json: A .json file is generated that can be fed directly into the NLP tool [prodigy](https://prodi.gy/).
- .html: A simple .html file is exported.
- Save & Restore: By using the button 'Serialize highlights' the annotator creates a text string containig all highlight(s) information. If you insert this string into the deserialize input, you can restore your highlights. 
- SKOS output: You can copy the XML-elements into the 'info' section of a concept in your [Cmap tools software](https://cmap.ihmc.us/).

## Known issues

- Restoring highlights: The 'Aisop-Web-Selection-Annotator' restores all highlights and their labels when the website is reloaded. However, it may happen that the highlights are 'shifted' in their position when reloading. Please test this before using the annotator productively. The solution is usually to set the constant `const MAINID = ""` (see configuration section) more precisely. This ID is in fact a prerequisite for the highlights to be restored correctly. If the problem persists, you can still use the annotator, but you will need to manually save and restore your progress (serialize, restore) or export the record.
- Opening another page: The 'Aisop-Web-Selection-Annotator' tries to restore all highlights when opening any page matching your @match definition. If you add highlights on page 'A', the highlights are also restored on page 'B'. This leads to messy results. For each new page you edit, you have to remove all old highlights with the button "delete all highlights" first.

## Third party libraries

The aisop-webselection-extractor uses the following third party libraries:

- jquery-3.6.1.js (DOM manipulation)
- popper.js (tooltips)
- micromodal.js (lightweight modal library)
- jquery-validation@1.19.5 (validation of forms)
- rangy.js (text selection and manipulation)

Please note: As you can see in the Greasemonkey script, some Javascript @require lines point to an Aisop server. This is due to the fact, that we extended the rangy library by some functionalites (see rangy-highlighter.js and rangy-classapplier.js). For simplicity, this was done within Rangy's files, but future versions should outsource the functionality. Pull-requests are very welcome. 