"""
This script reads a .json file that is the result of a prodigy annotation process.
The file is created after the annotation by using 'prodigy db-out'. The job of this
script is to iterate the annotated dataset and extract all annotated documents
that were given a specific label. This is e.g. helpful if you want to annotate 
sub-concepts. See: https://prodi.gy/docs/text-classification#large-label-sets

How to use:  
- type in terminal: python get-prodigy-docs-by-label.py
- You are asked to provide the relative path to the .json file to be processed
- You are asked to to provide the label by which the documents are to be extracted.
  Note: This is case sensitive!
- You are asked whether to split the documents' text by into sentences. Note: this
  will drastically reduce the length of the dataset bec. we have to drop most of 
  the metadata Prodigy added while annotation.
"""

LabelToExtractBy = "Encryption"
OutputPath = "./output.jsonl"
InputPath = "./input.jsonl"

import pandas as pd
import re
from rich import print

# Uncomment if you want to inspect the entire dataframe
# pd.set_option("display.max_rows", None)

jsonPath = input("\nPlease specify the path where the .json files can be found (default: "+InputPath+"): ") or InputPath
matchString = input("\nPlease specify the label to filter the documents by (default: "+LabelToExtractBy+"): ") or LabelToExtractBy
sentenceInput = input("\nDo you want to Split all texts to sentences [y/n] (default: y)? ") or "y"
toSentence = True if sentenceInput == 'y' else False

if toSentence:
    print("Alright, I'll split to sentences. That will take a bit longer.")
    import spacy
    nlp = spacy.load("de_core_news_md")
    nlp.select_pipes(enable=['tok2vec', "parser", "senter"])

print("Starting...")

# Read .jsonl file by given path
df = pd.read_json(jsonPath, lines=True)

# Select row where answer==accept and acceptLabel!=NaN
df = df[(df['answer'] == 'accept') & df['accept'].notnull()]

# Filter documents by given String
df = df[df['accept'].str.contains(matchString, regex=False)]

# If user wants the documents' text split to sentence, we have to
# abandon each documents' data besides 'text' and 'meta' property.
# This is bec. prodigy adds properties we cant duplicate, like 
# _input_hash and _task_hash...
if toSentence:
    rows = []
    for row in df.itertuples():
        text = row.text
        text = text.replace('\t', ' ').replace('\n', ' ')
        text = re.sub(r'\s+', ' ', text)

        doc = nlp(text)
        for sentence in doc.sents:
            text = sentence.text.strip()
            rows.append({
                "text": text,
                "meta": row.meta,
                "accept": row.accept,
            })
    df = pd.DataFrame.from_dict(rows)

# Write df to file using the same format as it was imported
df.to_json(OutputPath, orient='records', lines=True)

print("Wrote file to "+OutputPath)
