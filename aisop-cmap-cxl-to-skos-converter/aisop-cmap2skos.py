import re
from urllib import parse
from bs4 import BeautifulSoup
from titlecase import titlecase
import validators
import json
import textwrap

from rdflib import Graph, URIRef, Literal, Namespace, term
from rdflib.namespace import RDF, SKOS, DCTERMS, RDFS

# Get more information with debug mode on
DEBUG = False

# Globals
ImportPath = './import/example.cxl'
BaseUri = 'https://aisop.de/ontology/'

# https://rdflib.readthedocs.io/en/stable/plugin_serializers.html
# https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.Graph.serialize
ExportPath = 'export/example.ttl'
SerializeFormat = 'turtle'


# Parse long-comments
# IN CMap tools, you can add a 'long comment' to a node/concept. 
# This converter is able to parse the long comments provided they contain 
# XML-elements in SKOS-style. So, only enable the following flag if you want 
# to parse the concepts' long comments AND if they contain XML-elements in SKOS-style.
ParseComments = True

# The possible SKOS relations that the converter can extract from a Cmap.
# Abbreviations are defined here (e.g. n = narrower) and their symmetrical counterwords.
SkosRelations = {
    "narrower": { 
        "literal": ["n", "narrower", "spezifizischer"],
        "symmetric": "broader"
    },
    "broader": { 
        "literal": ["b", "broader", "allgemeiner"],
        "symmetric": "narrower"
    },
    "related": { 
        "literal": ["r", "related", "verwandt"],
        "symmetric": "related"
    }
}

# The converter is able to parse the 'info' (long-comment) of a Cmaps' nodes/concepts.
# So that users do not have to wrap their entries in <info>, the string is simply appended.
XmlPref = '<info xmlns:dcterms="http://purl.org/dc/terms/" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:dc="http://purl.org/dc/elements/1.1/" >'
XmlSuff = '</info>'

# The xml-nodes in the info block use namespaces like <skos:prefLabel>. When parsing
# the xml, we check if the used namespaces belong to the following namespaces.
ParseCommentNamespaces = {"rdf":RDF, "skos":SKOS, "dcterms":DCTERMS, "rdfs":RDFS}

def warning(text):
    print("--- Warning: "+text)

#----------------- CLASS "CONCEPT"-----------------
class Concept:

    rootConcept = None

    def __init__(self, c):
        self.id = c.get('id')
        self.parentId = c.get('parent-id')
        self.label = self.buildLabel(c.get('label'))
        self.uriLabel = self.buildUriLabel(self.label)
        self.uriRef = URIRef(self.uriLabel, BaseUri)
        self.shortComment = c.get('short-comment')
        self.longComment = c.get('long-comment')
        self.elements = self.parseComment(self.longComment)
        self.isRoot = False
        self.relations = {}

    def __eq__(self, other):
        return self.label == other.label
         
    def __hash__(self):
        return hash(self.label)
         
    def __str__(self):
        return self.label
        
    # Initial cleanage of a string
    # Returns a label ready for being a skos:label
    def buildLabel(self, label):
        label = label.strip()
        label = label.replace("\n", " ").replace("\r", " ")
        label = label.replace("- ", "-").replace("&", "and")
        label = re.sub(r"\s+", " ", label)
        if not label.isupper(): label = titlecase(label)
        label = label.encode('utf-8','ignore').decode("utf-8")
        return label

    # Returns a label which can be used in URI
    def buildUriLabel(self, label):
        label = label.lower().replace(" ", "-")
        label = re.sub('[^a-zA-Z0-9-]', '', label)
        return label

    def parseComment(self, comment):
        elements = []
        if not ParseComments: return elements
        if not comment: return elements
        comment = comment.replace("\n", "").replace("\r", " ")
        soup = BeautifulSoup(comment, features="lxml-xml")
        
        # long-comment contains text but not xml-style
        if not soup.contents: 
            warning("The concept with label '%s' has the following long-comment not in XML-format and is ignored: %s"%(self.label, textwrap.shorten(comment, width=50)))
            return elements

        # If the XML is valid, we re-parse it with XML-Prefix
        comment = XmlPref + comment + XmlSuff
        soup = BeautifulSoup(comment, features="lxml-xml")
        for c in soup.find_all():
            match = False
            if c.prefix in ParseCommentNamespaces:
                namespace = ParseCommentNamespaces[c.prefix]
                if c.name in namespace:
                    match = True
                    element = {
                        "namespace": namespace,
                        "name": c.name,
                        "lang": c.get("lang"),
                        "content": c.text.strip()
                    }
                elements.append(element)
            if not match and not c.name == 'info':
                warning("The concept with label '%s' contains the invalid XML-Tag '%s:%s' in its long-comment field. Did you add its namespace to 'ParseCommentNamespaces'?"%(self.label, c.prefix, c.name))
        return elements

    # We iterate all links. In case, our concept is the links' starting node (fromId), 
    # we store the target node (toId) in the respective relation (e.g. broader, narrower)
    def addRelations(self, links, concepts):
        for l in links:
            if self.id == l.fromId and l.toId:
                relatedConcept = concepts[l.toId]
                if l.relation in self.relations:
                    self.relations[l.relation].append(relatedConcept)
                else:
                    self.relations[l.relation] = [relatedConcept]                


    # Optimized SKOS vocabularies contain direct two-way links to speed up machine usage.
    # The converter accepts when concepts are only linked with e.g. 'narrower' and 
    # automatically derives the broader relation.
    def deriveRelations(self, concepts, links):
        for relation, relatedObjects in self.relations.items():
            if relation:
                symmetric = SkosRelations[relation].get('symmetric') 
                if not symmetric:
                    warning("There is no symmetric relation defined for '%s' in SkosRelations. Skipping!"%(relation))
                    continue
                for relatedObject in relatedObjects:
                    if symmetric not in relatedObject.relations:
                        relatedObject.relations[symmetric] = []
                    if self not in relatedObject.relations[symmetric]:
                        relatedObject.relations[symmetric].append(self)
            else:
                warning("Skipped concept with label '%s' as it does not have a relation."%(self.label))

    # Function checks whether a concept might be a root object
    # A concept is a possible root if it only has narrower connections but no broader ones.
    def possibleRoot(self):
        if 'broader' not in self.relations and 'narrower' in self.relations:
            return True

    # Function returns True if the concept is not linked to others
    def isUnlinked(self):
        if 'broader' not in self.relations and 'narrower' not in self.relations:
            return True

    # Function converts a root-concept to skos:ConceptScheme and adds it rdflib graph
    def toSkosConceptScheme(self, graph):
        graph.add((self.uriRef, RDF.type, SKOS.ConceptScheme))
        graph.add((self.uriRef, SKOS.prefLabel, Literal(self.label, lang=lang)))
        # The roots' concept relation 'narrower' is translated to skos:hasTopConcept
        for concept in self.relations.get('narrower'):
            graph.add((self.uriRef, SKOS.hasTopConcept, concept.uriRef))
        return graph

    # TODO: check if prefLabel is doubled!
    def toSkosConcept(self, graph, lang):

        # Create URI like 'https://aisop.de/ontology/internet'
        uriRef = URIRef(self.uriLabel, BaseUri)

        # Add concept to graph e.g.: <https://aisop.de/ontology/internet> a skos:Concept ;
        graph.add((uriRef, RDF.type, SKOS.Concept))

        # Add skos:prefLabel to this concept
        graph.add((uriRef, SKOS.prefLabel, Literal(self.label, lang=lang)))

        # Add narrower and broader relations to this concept
        # for this, iterate the concepts' relations (e.g. broader) and process the linked concepts
        # The dict looks like this: self.relations = {"broader": [concept1, concept2, concept3]}
        #print("\nLabel: %s (%s)"%(self.label, self.id))
        for relation, linkedConcepts in self.relations.items():
            #print(relation+":")
            for lc in linkedConcepts:
                lcUriRef = URIRef(lc.uriLabel, BaseUri)
                graph.add((uriRef, SKOS[relation], lcUriRef))
                #print("  %s (%s)"%(lcUriRef, lc.id))

        for e in self.elements:
            eNamespace = e.get('namespace')
            eName = e.get('name')
            eLang = e.get('lang')
            eContent = e.get('content')
            graph.add((uriRef, eNamespace[eName], Literal(eContent, lang=eLang)))

        return graph

#----------------- CLASS "LINK"-----------------
#This class describes a link between two Cmap concepts/nodes.
#The properties 'toId' and 'fromId' are the start and end nodes.
#The link thus is direct and does not use an intermediate node as Cmap does with its line labels.
class Link:

    def __init__(self, l):
        self.id = l.get('id')
        self.label = l.get('label')
        self.relation = self.getRelation(self.id, self.label)
        self.toId = None
        self.fromId = None

    def getRelation(self, lid, label):
        match = None

        for key, value in SkosRelations.items():
            if label.lower() in value.get('literal'): match = key
        if not match: 
            warning("The choosen label '%s' for linking-phrase id '%s' does not match Skos' valid relations."% (label, lid))
        return match

    def identifyConnections(self, cmapConnections, concepts):
        for cmapConnection in cmapConnections:
            fromId = cmapConnection.get('from-id')
            toId = cmapConnection.get('to-id')
            if self.id == fromId and toId in concepts:
                self.toId = toId
            if self.id == toId and fromId in concepts:
                self.fromId = fromId

#----------------- TERMINAL HELPER -----------------
# These methods help to keep the main program cleaner

# Function asks the user to confirm the language tag found in Cmap
# or set his/her own one. It validates the lang using rdflib function
# based on bcp47.
# See: https://www.rfc-editor.org/info/bcp47
# See: https://datatracker.ietf.org/doc/html/rfc3066.html
def getLanguageFromUser(metaLanguage):
    lang = None
    print("In SKOS, you can create multilingual elements or language-independent ones.")
    if metaLanguage:
        print("The converter found the following language in your Cmap: "+metaLanguage)
        while True:
            print("You can now decide wheter you want to:")
            print("1) Use '%s' language "%metaLanguage)
            print("2) Define another language")
            print("3) Use no language")
            choice = int(input("Your decision [default: 1]: ") or "1")
            if 1 <= choice < 4:
                if choice == 1: lang = metaLanguage
                elif choice == 2: lang = input("Type in your language: ")
                elif choice == 3: break
                if term._is_valid_langtag(lang): break
                else: warning("The choosen language is not valid. Make sure it follows this definition: https://www.rfc-editor.org/info/bcp47")
            else: 
                print("Incorrect input. Try again.")
    else:
        while True:
            print("The converter could not find any language in your Cmap. You can now: ")
            print("1) Define a language")
            print("2) Use no language")
            choice = int(input("Your decision [default: 1]: ") or "1")
            if 1 <= choice < 3:
                if choice == 1: lang = input("Type in your language: ")
                elif choice == 2: break
                if term._is_valid_langtag(lang): break
                else: warning("The choosen language is not valid. Make sure it follows this definition: https://www.rfc-editor.org/info/bcp47")
            else: 
                print("Incorrect input. Try again.")
    print("\nThe root concept was set to: '%s'.\n"%(str(lang)))
    return lang


def getRootFromUser(possibleRoot, concepts):
    # ROOT: Let the user choose the Cmaps' root node.
    if possibleRoot:
        while True:
            print("\nThe system has identified the following possible root concepts. Please confirm this by entering the corresponding number.")
            for i, r in enumerate(possibleRoot):
                print(str(i+1)+". Label: '"+r.label+"'; ID: "+r.id)
            choice = int(input("Your root [default: 1]: ") or "1") - 1 
            if 0 <= choice < len(possibleRoot):
                rootConcept = possibleRoot[choice]
                rootConcept.isRoot = True
                #Concept.rootConcept = possibleRoot[choice]
                break
            else: 
                print("Incorrect input. Try again.")
    else:
        while True:
            print("\nThe system could not identify a possible root concept. This is not ideal. However, you can specify a concept yourself. Please open your .cxl file and see the <concept-list>. Identify your root concept and copy the ID property like '1ZLDSP59L-GG6LGD-947'.")
            rootId = str(input("Your root ID: "))
            if rootId in concepts: 
                rootConcept = concepts[rootId]
                rootConcept.isRoot = True
                #Concept.rootConcept = concepts[rootId]
                break
            else: 
                print("This concept-ID does not exist. Try again.")
    print("\nThe root concept was set. Label: '%s', Cmap-ID: '%s'"% (rootConcept.label, rootConcept.id))
    return rootConcept

# This is only for debugging purposes.
# Method prints the concepts and relations to terminal.
def conceptsToTerminal(concepts):
    for concept in concepts.values():
        print("\nLabel: %s (%s)"%(concept.label, concept.id))
        for key, val in concept.relations.items():
            print(key+":")
            for obj in val:
                print("  %s (%s)"%(obj.label, obj.id))

#----------------- CMAP TO PY-OBJECTS -----------------
# In this step, we parse the Cmap-file and convert the xml-elements
# to Python objects of the classes 'Concept' and 'Link'. We then unify 
# the information so that the concept-objects perfectly reflect the 
# nodes and connections (links) in the Cmap. The converter then identifies
# unlinked nodes and helps the user to identifiy the root node.

print('\nOpening the file: '+ImportPath+"\n")

# Parse XML from a cmap exported to xml
with open(ImportPath) as file:
    soup = BeautifulSoup(file, features="lxml-xml")

# Access relevant areas of the cmap
cmap = soup.find('cmap')
meta = cmap.find('res-meta')
cmapConcepts = cmap.find('concept-list').find_all('concept')
cmapLinkPhrases = cmap.find('linking-phrase-list').find_all('linking-phrase')
cmapConnections = cmap.find('connection-list').find_all('connection')

# Get langauge from Cmap's meta section and let user confirm
metaLang = meta.find('dc:language').text.strip()
lang = getLanguageFromUser(metaLang)

# In this step we iterate the Cmap concept-list and build a Concept object each.
# We first only store the concepts info like id, label and comments. We skip duplicated labels.
# We create a dictionary to identify the concept-objects by their Cmap-ID later.

concepts = {} # identify concepts by their Cmap-ID
labelArr = [] # used to identify duplicates
for cmapConcept in cmapConcepts:
    # Check for duplicates
    label = cmapConcept.get('label')
    if label in labelArr: 
        warning("The concept with label '"+label+"' was skipped. It exists twice in your Cmap which is not allowed in SKOS. Either rename one concept or delete an occurrence and connect the remaining with a 'related' relation instead.")
        continue
    labelArr.append(label)
    # Create object of class Concept
    cId = cmapConcept.get('id')
    concepts[cId] = Concept(cmapConcept.attrs)

# Now, we iterate the Cmap <linking-phrase-list> and build a Link object each.
# We get the Link's relation (e.g. broader, narrower) and find the nodes its linking.
links = []
for cmapLinkPhrase in cmapLinkPhrases:
    link = Link(cmapLinkPhrase.attrs)
    link.identifyConnections(cmapConnections, concepts)
    links.append(link)

# Now, we add the found links to each respective concept-object.
for concept in concepts.values():
    concept.addRelations(links, concepts)
    concept.deriveRelations(concepts, links)

# Debug: print concepts and relations to terminal
if DEBUG: conceptsToTerminal(concepts)

# Check if there are any unlinked concepts
# Collect all possible root concepts
posRoot = []
for concept in concepts.values():
    if concept.isUnlinked():
        warning("The concept with label '%s' seems to have no 'narrower' or 'broader' connection to another concept."%(concept.label))
    if concept.possibleRoot(): posRoot.append(concept)

# Ask the user to confirm the root concept of the Cmap.
rootConcept = getRootFromUser(posRoot, concepts)
Concept.rootConcept = rootConcept


#----------------- BUILD RDF -----------------
# In this step we convert the concept-objects to SKOS.

# Create a RDF Graph using rdflib
graph = Graph()

# Bind the namespaces to a prefix
graph.bind('rdf', RDF)
graph.bind('skos', SKOS)
graph.bind('dct', DCTERMS)

# Convert root concept to RDF
rootConcept = Concept.rootConcept
graph = rootConcept.toSkosConceptScheme(graph)

# Convert all other concepts to RDF
for c in concepts.values():
    graph = c.toSkosConcept(graph, lang)

print("\n")
#print(graph.serialize(format=SerializeFormat))
#print(concepts.prettify())


#print("------------------------")

#----------------- RDF TO FILE -----------------
graph.serialize(destination=ExportPath, format=SerializeFormat, base=BaseUri)

print("Your Cmaps' .cxl file was successfully converted and saved to %s"%(ExportPath))
print("You should now validate the file with e.g. qSKOS (https://github.com/cmader/qSKOS) or skos-play (https://skos-play.sparna.fr/skos-testing-tool/)")