# CMap-CXL to SKOS converter

This python script converts a concept map to a SKOS ontology. Create your concept map using [CmapTools by IHMC](https://cmap.ihmc.us/), export it as .cxl and convert it to a SKOS ontology with this converter. 

## How to use

- Use `python -m pip install -r requirements.txt` to install the projects' dependencies.
- Run the script via `python aisop-cmap2skos.py`.

## Modify this script to your needs:

- Open `aisop-cmap2skos.py` and adjust the following variables for your needs: ImportPath, BaseUri, ExportPath and SerializeFormat. 
- You can run the script without altering the variables. The script will then use the file 'import/example.cxl'. Because this file explains some 'Do's and dont's', the converter will print some intended warnings to you. Please read the next section.

## How to design the CMap?

Please open the file "example.cmap" with the [CmapTools](https://cmap.ihmc.us/) software. It contains an examplary concept map that shows you how a Cmap should be constructed to be used with this converter. Please open the notes with a double click. They contain further instructions. You should also right-click on the concept at the bottom ("no xml in info") and read the "info".

## Known issues

- The following warning appears, although all relations were set correctly: "The concept with label 'x' seems to have no 'narrower' or 'broader' connection to another concept". Solution: Re-create the affected relation and export the Cmap again.
- The script suggests several nodes as root. Basically, all nodes that satisfy the following rule are suggested as a root: "A concept is a possible root if it only has narrower but no broader relations". If your Cmap fullfills this rule, you might re-create the relations to the wrongly suggested roots.
