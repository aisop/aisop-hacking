## Potential path

All text-extractions are done with `aisop-clipboard-extractor` which outputs a jsonl file to be put where prodigy is running.
All prodigy calls that create a UI create so on localhost port 8080 which is available from [prodigy1](https://dev.aisop.de/prodigy1/).

All commands are run in the prodigy virtual-env (`/home/prodigy/prodigy-venv`).


first clean-up a bit the datasets
`prodigy stats`, `prodigy stats -ls`, `prodigy db-in`, `prodigy dbout`, `prodigy drop`

first run manual on a set of basic inputs 

* e.g. extract all names of the topic map
* uses no model
* `prodigy textcat.manual fundid2 ~polx/alltopics.jsonl  --label ./topics.txt`
the run teach on a wider set

* e.g. extract lines from the slides-set and from the presenters notes
* maybe restricting labels by using an explicit `--label` because of context
* ` prodigy textcat.teach fundid2 de_dep_news_trf ./somefundidbits.jsonl --label Kodierung,Komprimierung,Programmierung`
* don't forget to save
check the input annotations by reviewing
* `prodigy review TODO`
the run the training

* `prodigy train outputmodel --textcat fundid2`
* (see [recipe](https://prodi.gy/docs/recipes/#training) for more parameters, e.g. qualiy eval sets)
then copy the model to the web-app
* currently in ` ~polx/model-attempts/`
in parallel import the CMap so it can be viewed

* @Alex? @Andreas?

then try on a portfolio

* import portfolio in [Mahara](https://mahara.aisop.de/) (from leap2a? @Andreas? @Alex?
* log-in to the web-app @Alex [hier?](https://dev.aisop.de/aisop/login/oauth1)




## Base Topics

Fundamental Principle
Operating System
Network
von Neumann Architecture
Representation
Data Structure
Classes & Objects
Coding
Encryption
Compression
Programming Concepts

==> Terms

Fundamental
Operating
Network
Neumann
Representation
Data-Structure
Classes
Objects
Coding
Encryption
Compression
Programming


====> Terms DE
Fundamental
Betriebsystem
Netzwerk
Neumann
Repräsentation
Datenstruktur
Klasse
Objekt
Kodierung
Verschlüsselung
Komprimierung
Programmierung



