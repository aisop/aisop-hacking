const util = require('util'); 
const fs = require('fs');
const xml2js = require('xml2js');

const IMPORT_PATH = "./import/aisop-fundamental-concepts-skos.cxl"
const EXPORT_PATH_JSON = "./export/aisop-fundamental-concepts-skos.cxl.json"
const EXPORT_PATH_TXT = "./export/aisop-fundamental-concepts-skos.cxl.txt"
const INCLUDE_STYLES = true;	// add styles to nodes?

var parser = new xml2js.Parser();

function removeWhites (label) {
	return label.replace(/\t/g, ' ').replace(/\n/g, ' ').replace(/\r/g, ' ').replaceAll("\\s{2,}", " ");
}

try {

	const data = fs.readFileSync(IMPORT_PATH, 'utf8');

	parser.parseStringPromise(data).then(function (result) {
		
		// Get cmap entries from <cmap><map>
		let cmap = result.cmap.map;

		// --------- CONCEPTS (NODES) ---------
		// Get concepts and map them as nodes[] array
		let nodes = cmap[0]['concept-list'][0].concept.map(c => c['$']);

		// Remove whitespacers from node labels
		nodes.forEach( function(n, i) { n.label = removeWhites(n.label) });

		// Each concept (node) has an appearance: position(x,y), dimensions (w,h), styles and optionally a stylesheet-id
		// We map these attributes to independent properties: node.position, node.dimension and node.styles
		if (INCLUDE_STYLES) {				
			let appearance = cmap[0]['concept-appearance-list'][0]['concept-appearance'].map(a => a['$']);
			appearance.forEach((a) => {
				nodes.forEach((n) => {
					if (a.id == n.id) {
						delete a.id;
						n.position = {};
						n.dimension = {};
						({ x:n.position.x, y:n.position.y, ...a } = a);
						({ width:n.dimension.width, height:n.dimension.height, ...a } = a);
						n.styles = a;
					}
				});
			});
		}
		
		/*
		//const a3 = appearance.map(t1 => ({...t1, ...nodes.find(t2 => t2.id === t1.id)}))
		const a3 = appearance.map(t1 => ({...t1, ...nodes.find(t2 => t2.id === t1.id)}))
		console.dir(a3)
		*/
		
		
		// --------- CONNECTIONS (LINKS) ---------
		// In Cmap, nodes are linked by 'linking-phrases', for example "is a" 
		// However, these phrases are represented as independent nodes, each heaving its own connection:
		// Eg: node1 -- c1 -- linking-phrase -- c2 -- node2

		// First we get all connections in a more readable format
		let connections = cmap[0]['connection-list'][0].connection.map(c => c['$']);

		// Now we get all phrases (e.g. "is a") in a more readable format stored in links[]
		let links = cmap[0]['linking-phrase-list'][0]['linking-phrase'].map(lp => lp['$']);

		// Now we iterate the links[] and see what connections they have using the phrase-id
		// We get the from-id from the starting node1 and the to-id from the target node2
		links.forEach((l, index, object) => {
			var source, target;
			connections.forEach((c) => {
				if (l.id == c['from-id']) { target = c['to-id']}
				if (l.id == c['to-id']) { source = c['from-id']}
			});
			if (source && target) {
				l.target = target;
				l.source = source;
			} else {
				console.log("----- A link that does not have either source or target ")
				console.dir(l)
				links.splice(index, 1);
			}
		});
		
		// Remove links that do not have target or source property
		links = links.filter(({target}) => target);
		links = links.filter(({source}) => source);

		// An object in links[] now looks like this, for example:
		// id: {'1YKK', label: 'is a', 'short-comment': '', 'long-comment': '', target: '1RW0', source: '1RW06'}

		// --------- STYLESHEET ---------
		let rawSheet = cmap[0]['style-sheet-list'][0]['style-sheet'];
		let sheetIds = rawSheet.map(s => s['$'])
		let sheetStyles = rawSheet.map(s => s['concept-style'][0]['$'])
		let stylesheet = sheetIds.map((el, i) => ({ ...el, ...sheetStyles[i] }));

		// Now only keep stylesheets which are actually used by a node
		// Note: by this, the styles of 'linking-phrase-appearance' are removed
		stylesheet = stylesheet.filter((element1) =>
			nodes.map((element2) => element2.styles['stylesheet-id']).includes(element1.id)
		);


		// --------- BUILD AND SAVE JSON ---------
		
		// Collect labels for .txt output
		let labels = nodes.map(c => c['label']);
		let labelsStr = labels.join(', ');

		fs.writeFile(EXPORT_PATH_TXT, labelsStr, err => {
			if (err) {throw err }
			console.log('TXT data is saved.')
		})

		let outputObj = {
			nodes: nodes,
			links: links,
			stylesheet: stylesheet
		}
		
		const data = JSON.stringify(outputObj)
		fs.writeFile(EXPORT_PATH_JSON, data, err => {
			if (err) {throw err }
			console.log('JSON data is saved.')
		})

	})
	.catch(function (err) {
		console.error("Error in parsing xml string");
		console.error(err);
	});

	/*

	const parser = new XMLParser();
	let parsedJson = parser.parse(data);
	let cmap = parsedJson.cmap.map;
	
	let nodes = {}
	let concepts = cmap['concept-list'];
	
	let that = cmap
	console.log(util.inspect(that, false, null))
	*/



} catch (err) {
	console.error(err);
}