# Cmap-XML to json and text converter

This tiny node script converts a Cmap (built in [CmapTools](https://cmap.ihmc.us/)) to a .json and .txt file.

## Export formats

This script converts a Cmap represented in a .xml file to two export formats:

- .json: The nodes, links and stylesheets are retained. The resulting .json file is ready to be used by e.g. D3, as it happens for example in our  [aisop-force-directed-graph](https://gitlab.com/aisop/aisop-hacking/-/tree/main/aisop-force-directed-graph).
- .txt: The resulting .txt file contains all concept names placed as nodes in your Cmap. The .txt file is meant to be used in a prodigy workflow where you hand in the concept names in order to e.g. annotate paragraphs in an NLP training process. 

## How to use

- Install Node.js (minimum V. 15) and run 'npm install' to install missing packages.
- Export your Cmap with the option "CXL file" in order to create a .xml file
- Copy the resulting .xml file to this repositories' input folder
- Open the 'xml-to-json-converter.js' file with your editor and update the import and export path matching your .xml filename (line 5-7).
- Run 'node xml-to-json-converter.js' to run the script.
- Open this repositories' export folder and see the results.

## Third party libraries

- xml2js: Simple XML to JavaScript object converter. It supports bi-directional conversion. Uses sax-js and xmlbuilder-js ([link](https://www.npmjs.com/package/xml2js)).