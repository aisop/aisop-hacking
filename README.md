# aisop-hacking

A few scripts to help into hacking for AISOP.
Please open the subfolders to read the corresponding documentations.

## About

This collection of tiny projects has been realized by the [AISOP team](https://aisop.de/team/) in order to support the following tasks:
- extract content from Mahara portfolios
- convert HTML content
- extract texts for prodigy annotations
- annotate and extract annotations results in the workflow of prodigy annotations
- work with spacy models
- ...
All attempts in this repository are considered temporary and not part of a maintainable product yet.
Please file issues on the repository for interest or comments

## License

Unless otherwise noted, files in this repository are under [Apache Public License](LICENSE.md).