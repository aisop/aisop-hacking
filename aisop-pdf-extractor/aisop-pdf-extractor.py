import math
from operator import itemgetter
import fitz # fitz is https://pymupdf.readthedocs.io
import json
import re
import pathlib

INPUT_DIR = "input/"
OUTPUT_DIR = "output/"

# The function uses a regex to remove hyphenation at the end of a line
# Param text: a string containing a hyphenated line break
# Examples: inspirie- ren --> inspirieren
def removeHyphenation(text):
    return re.sub(r'([a-z])-\s([a-z])', r'\1\2', text)

def removeNewlines(text):
    return text.replace("\\n"," ")

def removeMultipleSpaces(text):
    return re.sub(' +', ' ', text)

def removeLeadingString(text, arr):
    for s in arr: text = text.removeprefix(s).strip()
    return text
    
def removeTrailingString(text, arr):
    for s in arr: text = text.removesuffix(s).strip()
    return text

def removeInnerString(text, arr):
    for s in arr: text = text.replace(s, '')
    return text   

def arrayToJsonFile(filename, arr):
    jsonString = json.dumps(arr, ensure_ascii=False)
    jsonFile = open(filename, "w")
    jsonFile.write(jsonString)
    jsonFile.close()
    
def userChooseFile():
    choosenFile = ""
    availableFiles = list(pathlib.Path(INPUT_DIR).glob('*.pdf'))
    if not availableFiles:
        print("Could not find any files in", INPUT_DIR, "dir")
    else:
        nr = -1
        while (nr not in range(len(availableFiles))):
            for i, file in enumerate(availableFiles):
                print("\nChoose one of the following files by typing its number ")
                print(str(i)+": "+pathlib.Path(file).name)
                nr = input("File: ")
                try: nr = int(nr)
                except ValueError: print("This is not a number")
                    
        choosenFile = availableFiles[nr]
        print("You choose:",choosenFile)
        return choosenFile

def userChooseNumber(text):
    nr = -1
    while nr < 0:
        inp = input("\n"+text)
        try: int(inp)
        except: print("This is not a positive number")
        else: nr = int(inp)
    return nr

# Fn. returns a flag whether a list item was found and the cleaned string
def removeListCharacter(text):
    isList = False
    # Match "•" list item starting uppercase
    if (re.match("•\s{1,3}[A-Z]", text)):
        isList = True
        text = re.sub(r"•\s{1,3}", '', text, count = 1)
    # Match 1. and 1) list items
    elif(re.match("\d(.|\))\s{1,3}[A-Z]", text)):
        isList = True
        text = re.sub(r"\d(.|\))\s{1,3}", '', text, count = 1)
    # Match "-" list item 
    elif(re.match("-\s{1,3}[A-Z]", text)):
        isList = True
        text = re.sub(r"-\s{1,3}", '', text, count = 1)
    return isList, text
    
# Return true, if a unwanted string (list) occurs in a given text
def checkForPattern(text, patternArr):
    for pattern in patternArr:
        if re.match(pattern, text): return True

# This function returns an object in the required output format
def getOutputObj(options, pageNumber, blockText):
    meta = {"filename": options['filename'], "page": pageNumber}
    pageObj = {"document": blockText, "meta": meta }
    return pageObj
    
def processBookPages(book, options):
    
    # Array to collect processed book-page objects
    bookArr = []

    # iterate book pages in a user given range (start, end)
    pages = book.pages(options['start'],options['end'],1)
   
    for i, page in enumerate(pages):
        
        print(page.number + 1)
        
        # each page consists of multiple blocks, e.g. paragraph or list items
        # get all blocks which are inside the cropping/ clipping area (boundingBox)
        blocks = page.get_text("dict", clip = options['boundingBox'], sort = True)["blocks"]
        
        # iterate blocks of current page if they contain text (text == 0)
        # Use the 'lowest' level (span) to collect a blocks' text
        # See: https://pymupdf.readthedocs.io/en/latest/textpage.htm
        for block in blocks:
            blockText = ""
            if block['type'] == 0:
                for line in block["lines"]:
                    for span in line["spans"]:
                        spanText = span['text'].strip()
                        blockText += spanText + " "
            
            # Remove leading and trailing spaces and strings
            blockText = blockText.strip()
            blockText = removeLeadingString(blockText, options["removeLeading"])
            blockText = removeTrailingString(blockText, options["removeTrailing"])
            
            # If text is empty, we skip this loop run
            if not blockText: continue
            
            # Ignore this text block if it does not contain any of "mustContain" array of strings
            if options['mustContain'] and not any(s in blockText for s in options['mustContain']): continue
            
            # Ignore a block, if text starts with unwanted regex
            if(checkForPattern(blockText, options['ignorePattern'])): continue
            
            # If text contains list: get a flag and cleaned text
            isList, blockText = removeListCharacter(blockText)
            
            # Block text was collected, now do some cleanings
            blockText = removeHyphenation(blockText)
            blockText = removeMultipleSpaces(blockText)
            blockText = blockText.replace(' ,', '')
            
            # Now append text to array, considering the flags:
            
            # if text is list item and is not too long, we append it to previous text in array 
            if(isList and len(blockText) < options['listItemMax']):
                try: 
                    # append list items' text to previous text; continue with next loop run
                    bookArr[prevArrIndex]['document'] = bookArr[prevArrIndex]['document'] + "\n" + blockText
                    continue
                except: pass
            
            # IF text is not a list item AND is longer or equal the minimum character length
            # OR text is a list item but too lomg for prev. if claus so we store it as independent paragraph
            if(not isList and len(blockText) >= options['blocksLowerLimit'] or isList):
                pageObj = getOutputObj(options, page.number+1, blockText)
                bookArr.append(pageObj)
                prevArrIndex = len(bookArr) -1
        
    # Finally, return book array
    return bookArr
    
        
# -------------- MAIN --------------

# Get path and filename of pdf
pdfPath = userChooseFile()
pdfFilename = pathlib.Path(pdfPath).name

# let fitz (pymupdf) open the book
book = fitz.open(pdfPath)

# Calibration
# Set the cropping/bounding-box to crop the pages.
# The box  allows you only to parse a pages' main content ignoring header and footer
# Format is: x0, top, x1, bottom (a a square that is formed by a diagonal line)
boundingBox = fitz.Rect(40, 40, 499, 585)

# Print info about the mediabox/ bounding box for calibration
centerPage =  math.floor(book.page_count / 2)
print("\nFor calibration of the cropping box:")
print ("The text of this PDF seems to be in this rectangle:")
print(book.load_page(centerPage).rect)
print("In order to crop the pages, you have set this box:  ")
print(boundingBox)

# Let user enter page numbers to get the page range
startPage = userChooseNumber("Type in the start page (starting from 1): ") - 1
endPage = userChooseNumber("Type in the last page (starting from 1): ") - 1
pageRange = range(startPage,endPage+1, 1)

# Options passed to processBookPages function. See details below
ignorePattern = ["Abbildung\s{1,3}\d"]
removeLeading = ["Max. x."]
removeTrail = []
removeInStr = []
mustContain = ["Ruby "]

# Options for pdfplumber.open() function and string removal
options = {
    "path": pdfPath,                # path of pdf file
    "start": startPage,             # page number to start parsing
    "end": endPage,                 # page number to stop parsing
    "range": pageRange,             # deprecated
    "filename": pdfFilename,        # filename of pdf, e.g. test.pdf
    'listItemMax': 100,             # when a list item is stored as independent paragraph
    "boundingBox": boundingBox,     # rectangle to crop the pages
    "blocksLowerLimit": 30,         # ignore blocks shorter than this character count
    'ignorePattern': ignorePattern, # ignore blocks that match a specific pattern   
    "removeLeading": removeLeading, # array of strings to be removed from start of each paragraph
    "removeTrailing": removeTrail,  # array of strings to be removed from end of each paragraph           
    "removeInString": removeInStr,  # deprecated; array of strings to be removed inside of each paragraph           
    "mustContain": mustContain      # If array is set, only paragraphs with these strings are returned (OR)
}

# Open book, receive an array of parsed book-page-objects
bookArr = processBookPages(book, options)

# Store bookArr in json file
path = OUTPUT_DIR + pdfFilename + '.json'
arrayToJsonFile(path, bookArr)