from ast import If
import pdfplumber
import re
import json
import pathlib

INPUT_DIR = "input/"
OUTPUT_DIR = "output/"

RED = "\033[0;31m"
RESET_COLOR = '\033[0m'

# The function uses a regex to remove hyphenation at the end of a line
# Param text: a string containing "-\n", an indication of a hyphenated line break
# Return: a cleaned string where all occurences of "-\n" are removed
# Examples: Kompro-\nmiss --> Kompromiss; Buch-\nWebsite --> Buch-\nWebsite (no hyphenation present)
def removeHyphenation(text):
    return re.sub(r'([a-z])-\\n([a-z])', r'\1\2', text)

def removeNewlines(text):
    return text.replace("\\n"," ")

def removeMultipleSpaces(text):
    return re.sub(' +', ' ', text)

def removeLeadingString(text, arr):
    for s in arr: text = text.removeprefix(s)
    return text
    
def removeTrailingString(text, arr):
    for s in arr: text = text.removesuffix(s)
    return text

def removeInnerString(text, arr):
    for s in arr: text = text.replace(s, '')
    return text   

def arrayToJsonFile(filename, arr):
    jsonString = json.dumps(arr)
    jsonFile = open(filename, "w")
    jsonFile.write(jsonString)
    jsonFile.close()
    
def userChooseFile():
    choosenFile = ""
    availableFiles = list(pathlib.Path(INPUT_DIR).glob('*.pdf'))
    if not availableFiles:
        print("Could not find any files in", INPUT_DIR, "dir")
    else:
        nr = -1
        while (nr not in range(len(availableFiles))):
            for i, file in enumerate(availableFiles):
                print("\nChoose one of the following files by typing its number ")
                print(str(i)+": "+pathlib.Path(file).name)
                nr = input("File: ")
                try: nr = int(nr)
                except ValueError: print("This is not a number")
                    
        choosenFile = availableFiles[nr]
        print("You choose:",choosenFile)
        return choosenFile

def userChooseNumber(text):
    nr = -1
    while nr < 0:
        inp = input("\n"+text)
        try: int(inp)
        except: print("This is not a positive number")
        else: nr = int(inp)
    return nr
    
def getBook(options):
    bookCollectorArr = []
    
    with pdfplumber.open(options['path'], pages = options['range']) as pdf:
        for page in pdf.pages:
            #print("\nSeitenbreite", page.width)
            #print("Seitenhöhe", page.height, "\n")
            pageNr = str(page.page_number)
            try:
                # Crop the page to remove (invisible) header and footer
                page = page.within_bbox(options["boundingBox"], relative=False, strict=True)
            except Exception as e:
                print(RED+"Page",pageNr,"- Something went wrong:",e,RESET_COLOR)
            else:
                # Extract text and get it in a raw format
                text = page.extract_text()
                print(text)
                cleanText = '%r'%text 
                            
                # Clean the text
                cleanText = removeHyphenation(cleanText)
                cleanText = removeNewlines(cleanText)
                cleanText = removeMultipleSpaces(cleanText)
                cleanText = cleanText.strip('\'\"')
                
                # Remove user given leading/trailing/inside strings
                #cleanText = removeLeadingString(cleanText, options["removeLeading"])
                cleanText = removeTrailingString(cleanText, options["removeTrailing"])
                #cleanText = removeInnerString(cleanText, options["removeInString"])
                
                # Store page details in dictionary and append to array
                meta = {"filename": options['filename'], "page_number": page.page_number}
                pageObj = {"document": cleanText, "meta": meta }
                bookCollectorArr.append(pageObj)
                
                # Flush page cache
                page.flush_cache()
                
                print("Page",pageNr,"- Page extracted")
                 
        # Return book array
        return bookCollectorArr
    
    
        
# -------------- MAIN --------------

# Get path and filename of pdf
pdfPath = userChooseFile()
pdfFilename = pathlib.Path(pdfPath).name

# Let user enter page numbers to get the page range
startPage = userChooseNumber("Type in the start page: ")
endPage = userChooseNumber("Type in the last page: ")
pageRange = range(startPage,endPage+1, 1)

# tuple of x0, top, x1, bottom
boundingBox = (40, 140, 500, 700)

# Arrays used to remove leading/trailing strings from each page
# Such strings occur if editor add invisble elements
removeLeading = []
removeTrailing = ["Max. Linie", "Max. Max.", "Linie"]
removeInString = ["Max. "]

# Options for pdfplumber.open() function and string removal
# See https://github.com/jsvine/pdfplumber#loading-a-pdf
pdfOptions = {
    "path": pdfPath,
    "range": pageRange,
    "filename": pdfFilename,
    "boundingBox": boundingBox,
    "removeLeading": removeLeading,
    "removeTrailing": removeTrailing,
    "removeInString": removeInString
}

# Open book, receive an array of parsed book-page-objects
bookArr = getBook(pdfOptions)

# Store bookCollectorArr in json file
path = OUTPUT_DIR + pdfFilename + '.json'
arrayToJsonFile(path, bookArr)
