import pandas as pd
import csv

#Count,P,R,F,ROC
fundamental_principles = {
    "Algorithmization": [0,"0.0","0.0","0.0","None", 0],
    "Algorithm": [0,"0.0","0.0","0.0","None", 1],
    "Complexity": [0,"0.0","0.0","0.0","None", 1],
    "Program Flow": [0,"0.0","0.0","0.0","None", 1],
    "Programm": [0,"0.0","0.0","0.0","None", 1],
    "binary code": [0,"0.0","0.0","0.0","None", 1],
    "source code": [0,"0.0","0.0","0.0","None", 1],
    "Sorting": [0,"0.0","0.0","0.0","None", 1],
    "programming tools": [0,"0.0","0.0","0.0","None", 1],
    "interpreter": [0,"0.0","0.0","0.0","None", 2],
    "runtime environement": [0,"0.0","0.0","0.0","None", 2],
    "debugger": [0,"0.0","0.0","0.0","None", 2],
    "compiler": [0,"0.0","0.0","0.0","None", 2],
    "integrated development environment": [0,"0.0","0.0","0.0","None", 2],
    "programming concepts": [0,"0.0","0.0","0.0","None", 1],
    "Condition": [0,"0.0","0.0","0.0","None", 2],
    "propositional logic": [0,"0.0","0.0","0.0","None", 2],
    "proposition": [0,"0.0","0.0","0.0","None", 3],
    "compound proposition": [0,"0.0","0.0","0.0","None", 4],
    "atomic proposition": [0,"0.0","0.0","0.0","None", 4],
    "logical connective": [0,"0.0","0.0","0.0","None", 4],
    "conjuction": [0,"0.0","0.0","0.0","None", 5],
    "negation": [0,"0.0","0.0","0.0","None", 5],
    "disjunction": [0,"0.0","0.0","0.0","None", 5],
    "Test": [0,"0.0","0.0","0.0","None", 2],
    "Variable": [0,"0.0","0.0","0.0","None", 2],
    "Loop": [0,"0.0","0.0","0.0","None", 2],
    "Branching": [0,"0.0","0.0","0.0","None", 2],
    "Recursion": [0,"0.0","0.0","0.0","None", 2],
    "Classes and Objects": [0,"0.0","0.0","0.0","None", 0],
    "Data Structure": [0,"0.0","0.0","0.0","None", 1],
    "Static Data Structure": [0,"0.0","0.0","0.0","None", 1],
    "Array": [0,"0.0","0.0","0.0","None", 1],
    "Dynamic Data Structure": [0,"0.0","0.0","0.0","None", 1],
    "Linear List": [0,"0.0","0.0","0.0","None", 1],
    "Stack": [0,"0.0","0.0","0.0","None", 1],
    "Queue": [0,"0.0","0.0","0.0","None", 1],
    "Encoding": [0,"0.0","0.0","0.0","None", 0],
    "Compression": [0,"0.0","0.0","0.0","None", 0],
    "Lossy Encoding": [0,"0.0","0.0","0.0","None", 1],
    "Losseless Encoding": [0,"0.0","0.0","0.0","None", 1],
    "Run-length encoding": [0,"0.0","0.0","0.0","None", 1],
    "Huffman coding": [0,"0.0","0.0","0.0","None", 1],
    "redundancy": [0,"0.0","0.0","0.0","None", 1],
    "Computer Science": [0,"0.0","0.0","0.0","None", 0],
    "Encryption": [0,"0.0","0.0","0.0","None", 0],
    "Symmetric key systems": [0,"0.0","0.0","0.0","None", 1],
    "DES": [0,"0.0","0.0","0.0","None", 2],
    "Substitution ciphers": [0,"0.0","0.0","0.0","None", 2],
    "One-Time Pad": [0,"0.0","0.0","0.0","None", 2],
    "AES": [0,"0.0","0.0","0.0","None", 2],
    "Public key Systems": [0,"0.0","0.0","0.0","None", 2],
    "RSA Algorithm": [0,"0.0","0.0","0.0","None", 2],
    "PGP": [0,"0.0","0.0","0.0","None", 2],
    "Digital Signatures": [0,"0.0","0.0","0.0","None", 2],
    "Web of Trust": [0,"0.0","0.0","0.0","None", 2],
    "Private Key": [0,"0.0","0.0","0.0","None", 2],
    "Public Key": [0,"0.0","0.0","0.0","None", 2],
    "Error Control": [0,"0.0","0.0","0.0","None", 0],
    "error-tolerant code": [0,"0.0","0.0","0.0","None", 1],
    "checksum": [0,"0.0","0.0","0.0","None", 1],
    "parity bit": [0,"0.0","0.0","0.0","None", 1],
    "Cyclic Redundancy Check": [0,"0.0","0.0","0.0","None", 1],
    "Reed-Solomon code": [0,"0.0","0.0","0.0","None", 1],
    "error detection": [0,"0.0","0.0","0.0","None", 1],
    "error correction": [0,"0.0","0.0","0.0","None", 1],
    "Fundamental Principles": [0,"0.0","0.0","0.0","None", 0],
    "Criteria": [0,"0.0","0.0","0.0","None", 1],
    "Vertical Criterion": [0,"0.0","0.0","0.0","None", 2],
    "Horizontal Criterion": [0,"0.0","0.0","0.0","None", 2],
    "Criterion of Time": [0,"0.0","0.0","0.0","None", 2],
    "Criterion of Sense": [0,"0.0","0.0","0.0","None", 2],
    "Input-Process-Output Model": [0,"0.0","0.0","0.0","None", 0],
    "IT Security": [0,"0.0","0.0","0.0","None", 0],
    "IT Security Risks": [0,"0.0","0.0","0.0","None", 1],
    "Man-in-the-Middle Attack": [0,"0.0","0.0","0.0","None", 1],
    "Link": [0,"0.0","0.0","0.0","None", 0],
    "Network": [0,"0.0","0.0","0.0","None", 0],
    "Internet": [0,"0.0","0.0","0.0","None", 1],
    "Data Communication": [0,"0.0","0.0","0.0","None", 1],
    "Reference model": [0,"0.0","0.0","0.0","None", 1],
    "TCP/IP": [0,"0.0","0.0","0.0","None", 2],
    "ISO/OSI Reference model": [0,"0.0","0.0","0.0","None", 2],
    "protocol": [0,"0.0","0.0","0.0","None", 1],
    "IP": [0,"0.0","0.0","0.0","None", 2],
    "HTTP": [0,"0.0","0.0","0.0","None", 2],
    "TCP": [0,"0.0","0.0","0.0","None", 2],
    "Operating System": [0,"0.0","0.0","0.0","None", 0],
    "Resource": [0,"0.0","0.0","0.0","None", 1],
    "External Devices": [0,"0.0","0.0","0.0","None", 1],
    "File System": [0,"0.0","0.0","0.0","None", 1],
    "Memory": [0,"0.0","0.0","0.0","None", 1],
    "Process": [0,"0.0","0.0","0.0","None", 1],
    "Interface": [0,"0.0","0.0","0.0","None", 1],
    "User Interface": [0,"0.0","0.0","0.0","None", 1],
    "Console": [0,"0.0","0.0","0.0","None", 1],
    "GUI": [0,"0.0","0.0","0.0","None", 1],
    "Application Programming Interface": [0,"0.0","0.0","0.0","None", 1],
    "Scheduling": [0,"0.0","0.0","0.0","None", 1],
    "Layered Architecture": [0,"0.0","0.0","0.0","None", 1],
    "Programming Paradigm": [0,"0.0","0.0","0.0","None", 0],
    "declarative": [0,"0.0","0.0","0.0","None", 1],
    "object-oriented": [0,"0.0","0.0","0.0","None", 1],
    "procedural": [0,"0.0","0.0","0.0","None", 1],
    "Reflection": [0,"0.0","0.0","0.0","None", 0],
    "Representation": [0,"0.0","0.0","0.0","None", 0],
    "Number representation": [0,"0.0","0.0","0.0","None", 1],
    "IEEE 754": [0,"0.0","0.0","0.0","None", 2],
    "exponent": [0,"0.0","0.0","0.0","None", 3],
    "mantissa": [0,"0.0","0.0","0.0","None", 3],
    "sign": [0,"0.0","0.0","0.0","None", 3],
    "number system": [0,"0.0","0.0","0.0","None", 2],
    "denary number system": [0,"0.0","0.0","0.0","None", 3],
    "binary number system": [0,"0.0","0.0","0.0","None", 3],
    "two's complement": [0,"0.0","0.0","0.0","None", 4],
    "octal number system": [0,"0.0","0.0","0.0","None", 3],
    "hexadecimal number system": [0,"0.0","0.0","0.0","None", 3],
    "digit": [0,"0.0","0.0","0.0","None", 3],
    "base": [0,"0.0","0.0","0.0","None", 3],
    "Text representation": [0,"0.0","0.0","0.0","None", 1],
    "Unicode": [0,"0.0","0.0","0.0","None", 2],
    "UTF": [0,"0.0","0.0","0.0","None", 3],
    "ASCII": [0,"0.0","0.0","0.0","None", 2],
    "Version Control": [0,"0.0","0.0","0.0","None", 0],
    "Centralized Version Control System": [0,"0.0","0.0","0.0","None", 1],
    "SVN": [0,"0.0","0.0","0.0","None", 2],
    "CVS": [0,"0.0","0.0","0.0","None", 2],
    "Distributed Version control system": [0,"0.0","0.0","0.0","None", 1],
    "git": [0,"0.0","0.0","0.0","None", 2],
    "Merging": [0,"0.0","0.0","0.0","None", 2],
    "Repository": [0,"0.0","0.0","0.0","None", 1],
    "Virtualisation": [0,"0.0","0.0","0.0","None", 0],
    "Hypervisor": [0,"0.0","0.0","0.0","None", 1],
    "Virtual Machine": [0,"0.0","0.0","0.0","None", 1],
    "Von Neumann Architecture": [0,"0.0","0.0","0.0","None", 0],
    "Central Processing Unit": [0,"0.0","0.0","0.0","None", 1],
    "control unit": [0,"0.0","0.0","0.0","None", 1],
    "Arithmetic logical unit": [0,"0.0","0.0","0.0","None", 1],
    "bus system": [0,"0.0","0.0","0.0","None", 1],
    "input/output unit": [0,"0.0","0.0","0.0","None", 1],
    "memory": [0,"0.0","0.0","0.0","None", 1]
}

def main():
    #----------COUNT------------
    JSONfile = pd.read_json(path_or_buf="Input/fundid-all-l2-2025-01.jsonl", lines=True)
    accept = JSONfile.accept
    for serie in accept:
        for entry in serie:
            entry = entry.replace("|____ ", "")
            val = fundamental_principles.get(entry)
            val[0] = val[0]+1
            fundamental_principles[entry] = val
    # ----------PRF------------
    with open("Input/PRF.txt","r") as PRFFile:
        textlines = PRFFile.readlines()
        for text in textlines:
            text = text.replace("|____ ", "")
            text = text.replace("\t", "")
            text = text.replace("\n", "")
            Split = text.split("  ")
            str_list = list(filter(None, Split))
            val = fundamental_principles.get(str_list[0])
            val[1] = str_list[1].replace(" ","")
            val[2] = str_list[2].replace(" ", "")
            val[3] = str_list[3].replace(" ", "")
            fundamental_principles[str_list[0]] = val
    # ----------ROC------------
    with open("Input/ROC.txt","r") as PRFFile:
        textlines = PRFFile.readlines()
        for text in textlines:
            text = text.replace("|____ ", "")
            text = text.replace("\t", "")
            text = text.replace("\n", "")
            Split = text.split("  ")
            str_list = list(filter(None, Split))
            val = fundamental_principles.get(str_list[0])
            val[4] = str_list[1].replace(" ", "")
    # ----------Ausgabe------------ CSV
    with open('Output/fundid-all-l2-2025-01.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(["Label", "Count", "P", "R", "F", "ROC"])
        for Label, value in fundamental_principles.items():
            writer.writerow([Label, str(value[0]), value[1], value[2], value[3], value[4]])
    # ----------Ausgabe------------ txt
    with open("Output/fundid-all-l2-2025-01.txt", "w") as FileWriter:
        for Label, value in fundamental_principles.items():
            for i in range(value[5]):
                FileWriter.write("\t")
            FileWriter.write(Label + " " + str(value[0]) + " " + value[1]+ " " + value[2]+ " " + value[3]+ " " + value[4] + "\n")
    # ----------Ausgabe------------ html
    with open("Output/fundid-all-l2-2025-01.html", "w") as HtmlWriter:
        HtmlWriter.write(
            """<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>HTML Table</h2>

<table>
  <tr><th>Label</th><th>Count</th><th>P</th><th>F</th><th>R</th><th>ROC</th></tr>
  """)
        for Label, value in fundamental_principles.items():
            HtmlWriter.write("<tr><th>")
            for i in range(value[5]):
                HtmlWriter.write("&nbsp;&nbsp;&nbsp;&nbsp;")
            HtmlWriter.write(
                Label + "</th><th>" + str(value[0]) + "</th><th>" + value[1] + "</th><th>" + value[2] + "</th><th>" + value[3] + "</th><th>" + value[4] + "</th></tr>\n")
        HtmlWriter.write(
            """
</table>
</body>
</html>
            """)


if __name__ == '__main__':
    main()