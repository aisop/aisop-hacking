
L1 = {
    "Algorithmization": [],
    "Classes and Objects": [],
    "Encoding": [],
    "Compression": [],
    "Computer Science": [],
    "Encryption": [],
    "Error Control": [],
    "Fundamental Principles": [],
    "Input-Process-Output Model": [],
    "IT Security": [],
    "Link": [],
    "Network": [],
    "Operating System": [],
    "Programming Paradigm": [],
    "Reflection": [],
    "Representation": [],
    "Version Control": [],
    "Virtualisation": [],
    "Von Neumann Architecture": []
}


def main():
    with open("Input/fundid-all-l2-2025-01.jsonl","r") as JSONfile:
        textlines = JSONfile.readlines()
        for text in textlines:
            text = text.replace("|____ ", "")
            text = text.replace("--- ", "")
            L1label = text.split('"accept":["')[1].split('"')[0]
            #print(L1)
            val = L1.get(L1label)
            val = val + [text]
            L1[L1label] = val
    for L1Label, L1List in L1.items():
        if L1List:
            with open("Output/fundid-all-l2-2025-01_"+str(L1Label)+".jsonl", "w") as FileWriter:
                for text in L1List:
                    FileWriter.write(text)


if __name__ == '__main__':
    main()