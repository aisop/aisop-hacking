import json
from glob import glob
from pathlib import Path
import os.path

RESULTFILENAME = 'merged-json.json'

def mergeJsonFiles(jsonPaths, resultPath):
	mergedJson = []
	for jsonPath in jsonPaths:
		with open(jsonPath, 'r', encoding='utf-8-sig') as fileInput:
			mergedJson.extend(json.load(fileInput))

	with open(resultPath, 'w', encoding='utf-8-sig') as fileOutput:
		json.dump(mergedJson, fileOutput)

def findJsonInFilesystem(jsonPath, ignoreFile):
	jsonFiles = []
	jsonPath = os.path.join(jsonPath, '*.json')
	targetFiles = glob(jsonPath)
	for file in targetFiles:
		if not ignoreFile in file:
			jsonFiles.append(file)
	return jsonFiles

jsonPath = input("\nPlease specify the path where the .json files can be found (default: ./): ") or "./"

jsonFiles = findJsonInFilesystem(jsonPath, RESULTFILENAME)
print("Found these .json files:")
print ("\n".join(jsonFiles))

proceed = input("\nDo you want to merge these files (y/n)? ") or "y"
if proceed == "y":
	resultPath = os.path.join(jsonPath, RESULTFILENAME)
	mergeJsonFiles(jsonFiles, resultPath)
	print("The result was exported to "+resultPath)
else:
	print("Aborting.")