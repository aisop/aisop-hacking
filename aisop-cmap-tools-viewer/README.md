# Cmap Tools Viewer

A project using D3 library to visualize a Cmap in the web browser. It imports a .cxl file exported from [Cmap Tools](https://cmap.ihmc.us/). If you need a less sophisticated solution, you might give the '[Cmap Cloud](https://cmapcloud.ihmc.us/)' a try.

## Live Demo

We have developed the Cmap Viewer in the data visualization platform Observable. Find it [here](https://observablehq.com/@alexandergantikow/cmap-viewer). You can upload your own .cxl export by replacing our Cmap file. To do this, open the attachment section on the right-hand side and press "replace file". Please pay attention to the configuration notes and known limitations below.

## Background

This repository was developed in the context of the AISOP project. Our aim is to analyse so-called e-portfolios with the help of artificial intelligence, more precisely natural language processing (NLP), in order to support teachers and students at universities in evaluating and creating them. One use case is to find out which topics a student has covered in their e-portfolio. In order to illustrate the target state, we have modelled all topics of a seminar as a concept map (as a SKOS ontology) with the [Cmap Tools](https://cmap.ihmc.us/) software. This viewer makes it possible to display the concept map not only locally in the Cmap software but also as D3 visualisation in the web browser. This has the advantage that the visualisation can be enhanced with further user interactions. In our ['AISOP webapp'](https://gitlab.com/aisop/aisop-webapp), this is made possible by combining our NLP tools with the visualisation: The NLP tools analyze an e-portfolio and add the found to its HTML file. This file is displayed in the webapp next to this D3 visualisation. A user can click on a node in the visualisation to jump to the corresponding paragraph in the portfolio. 

## Run locally

- Install [git](https://git-scm.com/) if you haven't done yet.
- Open your terminal and clone this repository by using `git clone git@gitlab.com:aisop/aisop-hacking.git`
- Enter the project directory with `cd aisop-hacking/aisop-cmap-tools-viewer`
- Use an http-server to serve this project locally. You might use one of the following:
	- Npx: npx http-server
	- Node: npm install http-server and http-server
	- python3 -m http.server
	- ... or any web server of your favourite language :-)

## Configuration

There are numerous configuration options in the JavaScript file. Some only affect the appearance of the visualization, others have a functional character. The latter are described here:

| Variable | Description |
| ---      | ---      |
| IsHHierarchical (not yet implemented)  | If this setting is 'true', users can collapse and unfold the nodes of the visualisaton ([example](https://observablehq.com/@d3/collapsible-tree)) |
| ParseXml | If your Cmap concepts contain XML content (read below) in their info form, you can parse it be setting this to 'true'. You must set this to 'false', if your info contains normal text.
| SystemLang | If you set 'ParseXml' to true, the XML is parsed in each concepts' info form. As these xml elements can have various languages (e.g. attribute: lang="de"), you must set your language. This influences, which langauges' details are printed in the details section.

## XML content in your Cmap

In Cmap Tools, you can right-click a node to add a short and long comment to hit. However, this is quite limitating when you want to build a SKOS ontology. To come over this limitation, the Cmap Tools Viewer is able to parse any XML in your long-comment. You might want to inspect the provided example Cmap in the 'data' directory. As not all nodes contain XML yet, you can also visit this [example](https://gitlab.com/aisop/aisop-hacking/-/blob/main/aisop-cmap-cxl-to-skos-converter/example/example.cmap).

## Events

As already mentioned in the 'Background' section, the visualisation has been supplemented with additional user interactions. For example, a user can jump directly to a marker in a text by clicking on a node. However, to keep the project modular, it only contains the visualisation. Instead, it provides [custom events](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent) that can be listened to by another JavaScript (e.g. enabling the jump in the text). See the file 'js/custom-events.js' for an example usage of these events. The following events are provided:

| Event | Description | Details |
| ---      | ---      | ---      |
| concept-clicked   | A node was clicked. | event, data |
| concept-mouseover   | A node was hovered. | event, data |
| concept-mouseout   | The hovering of the node ended. | event, data |

## Related projects

- Cmap .cxl to SKOS converter: A converter to transfer concept map to a SKOS ontology. Create your concept map using CmapTools by IHMC, export it as .cxl and convert it to a SKOS ontology with this converter. [Link](https://gitlab.com/aisop/aisop-hacking/-/tree/main/aisop-cmap-cxl-to-skos-converte)
- Cmap-XML to json and text converter: This tiny node script converts a Cmap (built in CmapTools) to a .json and .txt file. [Link](https://gitlab.com/aisop/aisop-hacking/-/tree/main/aisop-cmap-xml-json-converter)
- Force directed Graph: By using the previous script, convert your Cmap to Json and visualize it with a force directed graph. [Link](https://gitlab.com/aisop/aisop-hacking/-/tree/main/aisop-force-directed-graph)

## Known issues

- All other lines/connection are drawn but one is missing: In Cmap Tools, please delete the connection an re-create it. You might also copy the style 

## Limitations

As the Cmap Viewer is still under construction, the following limitations must be taken into account:

- The styles of the nodes are used, but not the global stylesheets. The appearance must therefore be set for each node. To do this, right-click on a node, copy its format and paste it into the target node. If you are just starting your Cmap, define a node once and copy/paste it as soon as you need more nodes.
- Some styles of nodes, lines, rectangles and fonts are not yet converted.
- Images stored in the nodes are not converted.