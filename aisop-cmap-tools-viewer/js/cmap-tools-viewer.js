// ------------------------ DEFAULT/GLOBAL VARIABLES ------------------------

// The path to your exported Cmap .cxl file
const CmapPath = 'cmap/example.cxl'
	
// ID of <svg> element in index.html
const SvgId = "#cmap-viewer"

// SKOS
const IsHHierarchical = true    // not yet implemented
const ParseXml = true

// SKOS ontologies can be in different language. The used Cmap
// contains two languages: de and en
const SystemLang = "de"

// XML/SKOS namespace
let XmlPrefix = '<info xmlns:dcterms="http://purl.org/dc/terms/" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:dc="http://purl.org/dc/elements/1.1/">'
let XmlSuffix = '</info>'

// Default appearance/styles
const DefaultLinkBgColor = "237,244,246,255"
const DefaultFontFamily = "sans-serif"

const ScaleExtent = [1, 6]      // minimum/maximum allowed scale factor
const ZoomDuration = 500        // duration when using buttons to zoom in/out
const ScaleBy = 1.35            // scale factor when using buttons to zoom in/out
const ZoomConceptScale = 3      // zoom scale when zooming to concept by ID
const ZoomConceptTime = 2500    // duration in ms when zooming to concept by ID

// Marker/arrowhead
let MarkerRefX = 10     // x-offset of the marker from the target-point
let MarkerRefY = 0      // y-offset of the marker from the target-point
let MarkerWidth = 10;   // Width of the marker
let MarkerHeight = 10;  // Height of the Marker
let MarkerPoints = 'M 0,-5 L 10 ,0 L 0,5';
let MarkerViewBox = [0, -5, 10, 10]

// Mapping Cmaps' line styles (solid, dashed...) to D3/svg
const LineStyles = {
	"solid": ("0, 0"),
	"dashed": ("3, 3"),
	"dotted": ("1,1"),
	"dash-dot-dot": ("3, 3, 1, 1, 1, 1"),
	"dash-dot": ("3, 3, 1, 1")
}

// Mapping Cmaps' 'text-alignment' to a horizonal alignment (text-anchor)
// See: https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/text-anchor
const HorizontalAlign = {
	'left': 'start',
	'top-left': 'start',
	'bottom-left': 'start',
	'center': 'middle',
	'right': 'end',
	'top-right': 'end',
	'bottom-right': 'end'    
}

// Cmap IDs start with numbers which is not valid HTML. Therefore, we add a prefix.
const IdPrefix = "cmap-"

// A regex to split label texts by line breaks
const LineBreaker = new RegExp(/\n|\r/)

// We divide Cmaps 'border-shape-rrarc' property by belows value
const RadiusDivisor = 2

// Will contain the last clicked concept. Used to change its appearance.
var ClickedRect;
	
// ------------------------ HELPER ------------------------

// Cmap draws its concepts not from top/left corner but from its center
// This function helps to align the x/y coordinates of e.g. a rectangle
function center(coordinate, offset) {
	return parseInt(coordinate)-(parseInt(offset)/2)
}

// A line/connection can be positioned at various positions of the concept.
// Based on these positions, we need to adjust our start and end points.
function translateCoordinate(start, offset, fromPos, xORy) {
	start = parseInt(start)
	offset = parseInt(offset)
	
	if (fromPos == "center") {
		start=start+(offset/2)
	} else if (fromPos == "left") {
		if(xORy=="y") {start=start+(offset/2)}
	} else if (fromPos == "top-left") {
		// do nothing
	} else if (fromPos == "bottom-left") {
		if(xORy=="y") {start=start+offset}
	} else if (fromPos == "right") {
		start = xORy=="x" ? start+offset:start+(offset/2)
	} else if (fromPos == "top-right") {
		if(xORy=="x") {start=start+offset}
	} else if (fromPos == "bottom-right") {
		start = xORy=="x" ? start+offset:start+offset
	} else if (fromPos == "top") {
		if(xORy=="x") {start=start+(offset/2)}
	} else if (fromPos == "bottom") {
		start = xORy=="x" ? start+(offset/2) : start+offset
	}
	return start
}
// There are four arrow types: yes, no, if-to-concept, if-to-concept-and-slopes-up
// Depending on the arrow type and where the connection begins and ends, a decision 
// is made here as to whether an arrowhead is drawn. Function returns true/false?
function decideArrowDrawing(fromElement, toElement, cmapArrow) {
	let fromType = fromElement.attr('type')
	let toType   = toElement.attr('type')
	var arrow = false
	if(cmapArrow=="yes") {
		arrow = true
	} else if (fromType=="label" && toType=="concept" && cmapArrow=="if-to-concept") {
		arrow = true
	} else if (fromType=="label" && toType=="concept" && cmapArrow=="if-to-concept-and-slopes-up") {
		// If concept is lower than the label, arrow is not shown
		let distance = getRectDistance(fromElement, toElement)
		if(distance[1] <= 0) {arrow = true}      
	}
	return arrow    
}

// Function calculates the distance between two D3 elements
function getRectDistance(el1, el2) {
	let x1 = el1.attr("x")
	let y1 = el1.attr("y")
	let x2 = el2.attr("x")
	let y2 = el2.attr("y")
	return[x1-x2, y1-y2]
}

/**
   * Finds the intersection point between a rectangle and a line passing through.
   * This function was taken from Federico Destefanis (see link below) and
   * modified a bit.
   *
   * @param      {number}  rectWidth   The rectangle width
   * @param      {number}  rectHeight  The rectangle height
   * @param      {number}  rectX       Rectangle start x coordinate (not center!)
   * @param      {number}  rectY       Rectangle start y coordinate (not center!)
   * @param      {number}  pointX      The X-coordinate of the point.
   * @param      {number}  pointY      The Y-coordinate of the point.
   * @author     Federico Destefanis
   * @see        <a href="https://stackoverflow.com/a/70581013/5149302">original</a>
   * @see        <a href="https://stackoverflow.com/a/31254199/2668213">based on</a>
   * @return     {Object}  Returns the intersection point, e.g. {x:10, y:12}
   */  
function lineRectIntersection(rectWidth, rectHeight, rectX, rectY, pointX, pointY) {

	var w  = rectWidth/2;
	var h  = rectHeight/2;
	let xB = rectX + w;
	let yB = rectY + h; 
	var dx = pointX - xB;
	var dy = pointY - yB;

	//if A=B return B itself
	if (dx == 0 && dy == 0) return {x: xB, y: yB };

	var tan_phi = h / w;
	var tan_theta = Math.abs(dy / dx);

	//tell me in which quadrant the A point is
	var qx = Math.sign(dx);
	var qy = Math.sign(dy);

	if (tan_theta > tan_phi) {
		var xI = xB + (h / tan_theta) * qx;
		var yI = yB + h * qy;
	} else {
		var xI = xB + w * qx;
		var yI = yB + w * tan_theta * qy;
	}
	return {x: xI, y: yI};
}

function parseXmlFromString(str) {

	// Add namespace prefixes
	str = XmlPrefix + str + XmlSuffix

	// In good browser DOMParser, in IE ActiveXObject
	if (window.DOMParser) {
			let parser = new DOMParser();
			var doc = parser.parseFromString(str, "application/xml");
	} else {
			var doc = new ActiveXObject("Microsoft.XMLDOM");
			doc.async = false;
			doc.loadXML(str);
	}
	// On error, on success
	const errorNode = doc.querySelector("parsererror");
	if (errorNode) { 
		console.error("An error occured while parsing XML")
		return false;
	}  else {
		return doc
	} 
}

function getDataFromXmlDoc(doc) {
	let nodeList = doc.getElementsByTagName("info")[0].childNodes;
	let nodes = Array.from(nodeList).filter(n=> n.nodeType !== Node.TEXT_NODE)
	return nodes
}

// Function prints concept details to DOM
// It retreives data from e.g. a clicked concept node
function printConceptDetails(data, skosXmlNodes = []) {

	// Empty list
	let dd = document.getElementById("concept-details").querySelectorAll('dd')
	for (const d of dd) {d.innerHTML = ""}
	
	document.getElementById("concept-label-dd").innerHTML = data.getAttribute('label');

	for (const node of skosXmlNodes) {
		if(SystemLang == node.getAttribute("lang")) {
			document.getElementById(node.tagName).innerHTML = node.textContent;
		}
	}	
}

// ------------------------ D3 CHART ------------------------

d3
	// Read .cxl file and build the graph
	.xml(CmapPath)
	.then((xml) => {
		
		// Selecting the <svg> element in index.html
		var svg = d3.select(SvgId)
		
		// Get data from the XML object
		// const conceptsWithAppearances maps concepts and their appearance together
		const map = xml.documentElement.getElementsByTagName("map")[0]
		const concepts = xml.documentElement.getElementsByTagName("concept")
		const conceptAppearances = xml.documentElement.getElementsByTagName("concept-appearance")
		const conceptsWithAppearances = Array.from(concepts).map((c) => ({c:c, a: conceptAppearances.namedItem(c.id)}))
		const linkingPhrases = xml.documentElement.getElementsByTagName("linking-phrase")
		const linkingPhrasesAppearance = xml.documentElement.getElementsByTagName("linking-phrase-appearance")
		const connections = xml.documentElement.getElementsByTagName("connection")
		const connectionAppearances = xml.documentElement.getElementsByTagName("connection-appearance")

		// Get width and height of entire Cmap
		const mapWidth = map.getAttribute("width");
		const mapHeight = map.getAttribute("height");

		// Create zoom behaviour; attach event handler
		// See https://d3js.org/d3-zoom and https://www.d3indepth.com/zoom-and-pan/
		const zoom = d3.zoom()
				.extent([[0, 0], [mapWidth, mapHeight]])
				.scaleExtent(ScaleExtent)
				.on("zoom", handleZoom)

		// Function is fired on 'zoom' event (e.g. mouse wheel)
		function handleZoom(e) {
			svg.selectChildren('g').attr('transform', e.transform);
		}

		function zoomIn() {
			zoom.scaleBy(svg.transition().duration(ZoomDuration), ScaleBy)
		}

		function zoomOut() {
			zoom.scaleBy(svg.transition().duration(ZoomDuration), 1/ScaleBy)
		}
		
		// Function resets the zoom
		function zoomReset() {
			svg.transition()
				.duration(750)
				.call(zoom.transform, d3.zoomIdentity);
		}

		// Function zooms to Concept by coordinates
		function zoomToConcept(x,y) {
			svg.transition().duration(ZoomConceptTime).call(
				zoom.transform,
				d3.zoomIdentity.translate(mapWidth/2, mapHeight/2).scale(ZoomConceptScale).translate(-x, -y)
			);
		}

		/**
		 * Function is called when a concept is clicked. It changes the class of
		 * the previous/current clicked concept. It fires the 'concept-clicked'
		 * custom event which can be catched from other JavaScripts.
		 *
		 * @param      {object}  e       D3 event
		 * @param      {object}  data    D3 data of the clicked concept
		 */
		function clickConcept(e, data) {
			e.stopPropagation()
			if(ClickedRect!==undefined) {ClickedRect.classed('cmap-concept-active', false)}
			ClickedRect = d3.select( "#"+IdPrefix+data.c.id)
			ClickedRect.classed('cmap-concept-active', true)
			zoomToConcept(data.a.getAttribute('x'), data.a.getAttribute('y'))
			
			var lc = data.c.getAttribute('long-comment')
			var skosNodesArr = []
			if(ParseXml) {
				var doc = parseXmlFromString(lc)
				skosNodesArr = getDataFromXmlDoc(doc)
			}
			printConceptDetails(data.c, skosNodesArr)
			// Dispatch custom event
			window.dispatchEvent(new CustomEvent("concept-clicked", {detail:data}));
		}

		function mouseoverConcept(e, data) {
			e.stopPropagation()
			let rect = d3.select( "#"+IdPrefix+data.c.id)
			rect.classed('cmap-concept-mouseover', true)
			window.dispatchEvent(new CustomEvent("cmap-concept-mouseover", {detail:data}));
		}

		function mouseoutConcept(e, data) {
			e.stopPropagation()
			let rect = d3.select( "#"+IdPrefix+data.c.id)
			rect.classed('cmap-concept-mouseover', false)
			window.dispatchEvent(new CustomEvent("concept-mouseout", {detail:data}));
		}

		// Configure <svg> element
		svg
		.attr("viewBox", [0, 0, mapWidth, mapHeight])
		.attr("width", mapWidth)
		.attr("height", mapHeight)
		.attr("style", "max-width: 100%; height: auto;")
		.call(zoom)
		
		// Layers: The earlier here, the lower in svg
		var connectionsGroup = svg.append("g"); // original position
		var conceptsGroup = svg.append("g");
		var conceptsLabelsGroup = svg.append("g");
		var linksGroup = svg.append("g");
		var linksLabelsGroup = svg.append("g");
		
		// Create a group for each cmap/xml concept
		// Then add a rectangle and multiline text to each group
		conceptsGroup
			.classed('cmap-concepts', true)
			.selectAll()
			.data(conceptsWithAppearances)
			.join("g")
			.classed('cmap-concept', true)
			.on("click", clickConcept)
			.on('mouseover', mouseoverConcept)
			.on('mouseout', mouseoutConcept)
			.append('rect')
				.attr("id", c => IdPrefix+c.a.id)
				.attr("type", "concept")
				.attr("label", c => c.c.getAttribute("label"))
				.attr("x", c => center(c.a.getAttribute("x"), c.a.getAttribute("width")))
				.attr("y", c => center(c.a.getAttribute("y"), c.a.getAttribute("height")))
				.attr('width', c => c.a.getAttribute("width"))
				.attr('height', c => c.a.getAttribute("height"))
				.attr("rx", c => c.a.getAttribute("border-shape-rrarc") / RadiusDivisor)
				.attr("ry", c => c.a.getAttribute("border-shape-rrarc") / RadiusDivisor)
				.attr('fill', c => d3.color(`rgba(${c.a.getAttribute("background-color")})`))
				.attr('stroke', c => d3.color(`rgba(${c.a.getAttribute("border-color")})`))
				.attr("stroke-width", c => c.a.getAttribute("border-thickness"))
			.select(function() { return this.parentNode })
			.append("text")
				.attr("type", "concept-label")
				.attr("font-family", c => c.a.getAttribute("font-name")+', '+DefaultFontFamily)
				.attr("font-size", c => c.a.getAttribute("font-size"))
				.attr("x", c => center(c.a.getAttribute("x"), c.a.getAttribute("width")))
				.attr("y", c => center(c.a.getAttribute("y"), c.a.getAttribute("height")))
				.attr("text-anchor", "middle")
				.attr("dominant-baseline", "central")
				.selectAll()
				.data((c) => {
					let split = concepts.namedItem(c.a.id).getAttribute("label").split(LineBreaker)
					return split.map((s) => ({s:s, x:c.a.getAttribute("x"), y:c.a.getAttribute("y"), id:c.a.getAttribute("id")}));
				})        
				.join('tspan')
					.text(t => t.s)
					.attr("x", t => t.x)
					.attr("dy", "1.1em")   

		// Add a rectangle for each linking-phrase appearance
		linksGroup
			.selectAll()
			.data(linkingPhrasesAppearance)
			.join("rect")
				.attr("id", a => IdPrefix+a.getAttribute("id"))
				.attr("type", "label")
				.attr("label", a => linkingPhrases.namedItem(a.id).getAttribute("label"))
				.attr("x", a => center(a.getAttribute("x"), a.getAttribute('width')))
				.attr("y", a => center(a.getAttribute("y"), a.getAttribute('height')))
				.attr('width', a => a.getAttribute("width"))
				.attr('height', a => a.getAttribute("height"))
				.attr("rx", a => a.getAttribute("border-shape-rrarc"))
				.attr("ry", a => a.getAttribute("border-shape-rrarc"))
				.attr('fill', a => d3.color(`rgba(${a.getAttribute("background-color")})`))
				.attr('fill', (a) => {
					let bc = a.getAttribute("background-color")
					if(!bc) {bc = DefaultLinkBgColor}
					return d3.color(`rgba(${bc})`)
				})
				.attr('stroke', a => d3.color(`rgba(${a.getAttribute("border-color")})`))
			;
		
		// Add a text label for each linking phrase
		linksLabelsGroup
			.selectAll()
			.data(linkingPhrasesAppearance)
			.join("text")
				.classed('cmap-linklabel',true)
				.attr("type", "labeltext")
				.attr("font-family", a => a.getAttribute("font-name")+', '+DefaultFontFamily)
				.attr("font-size", a => a.getAttribute("font-size"))
				.attr("x", a => a.getAttribute("x"))
				.attr("y", a => a.getAttribute("y"))
				.attr("text-anchor", "middle")
				.attr("dy", "0.35em")
				.text(a => linkingPhrases.namedItem(a.id).getAttribute("label"));

		// Add a path/line for each connection
		for (let connection of connections) {

			// The id of the connection and its appearance
			let cId = connection.getAttribute("id")
			let cApp = connectionAppearances.namedItem(cId)
			
			// We select the concepts to be connected from the svg
			// You could also get info about them from the xml
			let fromSelector = "#"+IdPrefix+connection.getAttribute('from-id')
			let fromElement = svg.select(fromSelector)
			let toSelector = "#"+IdPrefix+connection.getAttribute('to-id')
			let toElement = svg.select(toSelector)

			// Start and end points of the elements to be connected
			let fromElemX = parseInt(fromElement.attr('x'))
			let fromElemY = parseInt(fromElement.attr('y'))
			let toElemX   = parseInt(toElement.attr('x'))
			let toElemY   = parseInt(toElement.attr('y'))

			// Height and width of the elements to be connected
			let fromElemWidth  = parseInt(fromElement.attr('width'))
			let fromElemHeight = parseInt(fromElement.attr('height'))
			let toElemWidth    = parseInt(toElement.attr('width'))
			let toElemHeight   = parseInt(toElement.attr('height'))

			// A line/connection can be positioned at various positions of the concept.
			// Based on these positions, we need to adjust our start and end points.
			let fromPos = cApp.getAttribute('from-pos')
			let toPos = cApp.getAttribute('to-pos')
			let fromX = translateCoordinate(fromElemX, fromElemWidth, fromPos, "x")
			let fromY = translateCoordinate(fromElemY, fromElemHeight, fromPos, "y")
			let toX = translateCoordinate(toElemX, toElemWidth, toPos, "x")
			let toY = translateCoordinate(toElemY, toElemHeight, toPos, "y")
			
			// Start to collect the coordinates in order to build the path
			// Add 'start' point, additional control points and 'end' point
			let points = [[fromX,fromY]]
			let controlPoints = cApp.getElementsByTagName("control-point")
			for (let c of controlPoints) points.push([c.getAttribute('x'), c.getAttribute('y')]);
			points.push([toX,toY])

			// With these points, the Cmap concepts can be connected by lines/paths
			// However, some connections are tied to the centre of the node.
			// We have to check the connections for overlaps and adjust their coordinates. 
			// Special attention must be paid to the additional contol points. 
			// We therefore work with the second and second-last points.

			// A line starting in the second point, intersecting the 'from' element. Update the first point.
			let pointFrom = lineRectIntersection(fromElemWidth, fromElemHeight, fromElemX, fromElemY, points[1][0], points[1][1])
			points[0] = [pointFrom.x, pointFrom.y]

			// A line starting from the second-last point, intersecting the 'to' element. Update the last point.
			let scndLast = points[points.length-2];
			let pointTo = lineRectIntersection(toElemWidth, toElemHeight, toElemX, toElemY, scndLast[0], scndLast[1])
			points[points.length-1] = [pointTo.x, pointTo.y]


			// ----------------------- ARROW -----------------------
			
			// Get name/type of connection's arrow, e.g. "if-to-concept"
			// Decid if arrow is to be drawn, depending on elements and arrow type
			let cmapArrow = cApp.getAttribute("arrowhead")
			let arrow = decideArrowDrawing(fromElement, toElement, cmapArrow)
			
			// The arrowhead
			if(arrow) {
				connectionsGroup
					.append('defs')
					.append('marker')
					.attr('id', `arrow-${cId}`)
					.attr('viewBox', MarkerViewBox)
					.attr('refX', MarkerRefX)
					.attr('refY', MarkerRefY)
					.attr('markerWidth', MarkerWidth)
					.attr('markerHeight', MarkerHeight)
					.attr('orient', 'auto-start-reverse')
					.append('path')
					.attr('d', MarkerPoints)
					.attr('stroke', 'black');      
			}
		
			// Finally draw the path/line with arrow
			let markerEnd = arrow ? `url(#arrow-${cId})` : "none"
			connectionsGroup
				.append('path')
				.attr("type", "connection")
				.attr('d', d3.line()(points))
				.style("stroke-dasharray", LineStyles[cApp.getAttribute("style")])
				.attr('stroke', d3.color(`rgba(${cApp.getAttribute("color")})`))
				.attr('marker-end', markerEnd)
				.attr('fill', 'none');    
		}

		// Event listeners: Zoom Buttons
		let zoomForm = document.getElementById('zoom-form')
		zoomForm.zoomIn.onclick = zoomIn;
		zoomForm.zoomOut.onclick = zoomOut;
		zoomForm.zoomReset.onclick = zoomReset;

		return svg.node();

	});