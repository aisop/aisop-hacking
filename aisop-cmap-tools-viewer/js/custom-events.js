window.addEventListener("concept-clicked", (e) => {
  console.log("The custom event 'concept-clicked' was fired. These are the details sent by the event: ", e.detail);
});

window.addEventListener("concept-mouseover", (e) => {
  console.log("The custom event concept-mouseover' was fired. These are the details sent by the event: ", e.detail);
});

window.addEventListener("concept-mouseout", (e) => {
  console.log("The custom event concept-mouseout' was fired. These are the details sent by the event: ", e.detail);
});