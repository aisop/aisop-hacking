import os
import json
import re
from rich import print

# A line must have at least 'n' letters
LetterTreshold = 5

def get_import_path():
    while True:
        import_path = input("Please provide an import path (Default: import/): ") or "import/"
        if os.path.isdir(import_path):
            jsonl_files = [f for f in os.listdir(import_path) if f.endswith('.jsonl')]
            if jsonl_files:
                return import_path, jsonl_files
            else:
                print("[red]No .jsonl files found in the specified directory. Please provide a correct path.[/red]")
        else:
            print("[red]The specified path is not a valid directory. Please provide a correct path.[/red]")

def select_jsonl_file(jsonl_files):
    while True:
        print("Found .jsonl files:")
        for idx, file in enumerate(jsonl_files):
            print(f"{idx}: {file}")
        
        try:
            file_index = int(input("Please enter the number of the file to process: "))
            if 0 <= file_index < len(jsonl_files):
                return jsonl_files[file_index]
            else:
                print("[red]Invalid number. Please select a valid number.[/red]")
        except ValueError:
            print("[red]Invalid input. Please enter a number.[/red]")

def read_jsonl_file(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            return [json.loads(line) for line in file]
    except Exception as e:
        print(f"[red]Error opening the file: {e}[/red]")
        return None

def extract_labels(objects):
    labels = set()
    for obj in objects:
        if "accept" in obj and isinstance(obj["accept"], list):
            labels.update(obj["accept"])
    return sorted(labels)

def filter_objects_by_label(objects, labels):
    results = {label: [] for label in labels}
    for obj in objects:
        if "accept" in obj:
            for label in labels:
                if label in obj["accept"]:
                    filtered_obj = {
                        "meta": obj.get("meta", {}),
                        "html": obj.get("html", ""),
                        "text": obj.get("text", "")
                    }
                    results[label].append(filtered_obj)
    return results

def sanitize_filename(filename):
    return re.sub(r'\s+', '-', re.sub(r'[^\w\s-]', '', filename))

def split_text_objects(objects):
    split_objects = []
    for obj in objects:
        text_lines = obj["text"].split('\n')
        for idx, line in enumerate(text_lines):
            if len(line) <= LetterTreshold: continue
            split_obj = {
                "meta": obj.get("meta", {}).copy(),
                "text": line
            }
            if "document" in split_obj["meta"]:
                split_obj["meta"]["document"] = f"{split_obj['meta']['document']}_split_{idx}"
            else:
                split_obj["meta"]["document"] = f"split_{idx}"
            split_objects.append(split_obj)
    return split_objects

def main():
    import_path, jsonl_files = get_import_path()
    selected_file = select_jsonl_file(jsonl_files)
    file_path = os.path.join(import_path, selected_file)
    
    objects = read_jsonl_file(file_path)
    if objects is None:
        print("[red]No annotations found.[/red]")
        return

    labels = extract_labels(objects)
    if not labels:
        print("[red]No available labels found.[/red]")
        return

    print("[bold]Available labels:[/bold]")
    for idx, label in enumerate(labels):
        print(f"{idx}: {label}")

    while True:
        try:
            label_input = input("Please enter the number of the label or 'all' for all labels: ").strip().lower()
            if label_input == 'all':
                selected_labels = labels
                break
            else:
                label_index = int(label_input)
                if 0 <= label_index < len(labels):
                    selected_labels = [labels[label_index]]
                    break
                else:
                    print("[red]Invalid number or input. Please select a valid number or 'all'.[/red]")
        except ValueError:
            print("[red]Invalid input. Please enter a number or 'all'.[/red]")

    results = filter_objects_by_label(objects, selected_labels)

    split_choice = input("Do you want to split the texts by line breaks? [y/n] (Default: y): ").strip().lower() or "y"
    if split_choice == "y":
        for label in selected_labels:
            if results[label]:
                results[label] = split_text_objects(results[label])

    if any(results.values()):
        for label, result_list in results.items():
            if result_list:
                sanitized_label = sanitize_filename(label)
                filename, ext = os.path.splitext(selected_file)
                output_filename = f"{filename}_{sanitized_label}{ext}"
                output_path = os.path.join(import_path, output_filename)
                try:
                    with open(output_path, 'w', encoding='utf-8') as outfile:
                        for result in result_list:
                            outfile.write(json.dumps(result) + '\n')
                    print(f"[green]Results for label '{label}' successfully saved to {output_path}[/green]")
                except Exception as e:
                    print(f"[red]Error saving the results for label '{label}': {e}[/red]")
    else:
        print("[red]No results found.[/red]")

if __name__ == "__main__":
    main()
