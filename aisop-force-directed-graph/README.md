# Serve it locally

Simple opening the index.html is not sufficient. Use an http-server to serve this project locally. You might use:
- `npx http-server`
- `npm install http-server` and `http-server`
