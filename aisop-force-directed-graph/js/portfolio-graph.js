// TODOS
// - Maybe use polyfill for scrollIntoView: https://www.npmjs.com/package/seamless-scroll-polyfill



// Copyright 2021 Observable, Inc.
// Released under the ISC license.
// https://observablehq.com/@d3/force-directed-graph
// Code originally provided by Mike Bostock, edited May 16
// Some modifications were provided by a fork of @shreshthmohan

/*
----- Credits-----
- This graph uses "d3-force-reuse" (forceManyBodyReuse) by Robert Gove from https://github.com/twosixlabs/d3-force-reuse
  See: It Pays to Be Lazy: Reusing Force Approximations to Compute Better Graph Layouts Faster." In Proceedings of the 11th Forum Media Technology (2018), pp. 43-51.
- This graph uses...
*/

function ForceGraph(
  nodes,                        // an iterable of node objects (typically [{id}, …])
  links,                        // an iterable of link objects (typically [{source, target}, …])
  {                             // an object of the following options
    svg,                        // the target element
    width = 640,                // outer width of the graph svg in px
    height = 400,               // outer heightof the graph svg in px
    colors = d3.schemeTableau10,// an array of color strings, for the node groups

    // Data
    mainContentId,              // the id of the content container (string, without #)
    nodeId = d => d.id,         // given data in nodes, returns a unique identifier (string)
    nodeGroup,                  // given data in nodes, returns an (ordinal) value for color
    nodeGroups,                 // an array of ordinal values representing the node groups
    nodeLabel,                  // given data in nodes, a title string
    nodeStyles,                 // given data in nodes, an object with style info
    nodeShortComment = d => d['short-comment'], // given data in nodes, a short comment string
    nodeLongComment  = d => d['long-comment'],  // given data in nodes, a long comment string
    linkShortComment = d => d['short-comment'], // given data in links, a short comment string
    linkLongComment  = d => d['long-comment'],  // given data in links, a short comment string
    //linkLongComment,            // given data in links, a long comment string
    
    linkSource = ({source}) => source, // given data in links, returns a node identifier string
    linkTarget = ({target}) => target, // given data in links, returns a node identifier string
    linkType = ({type}) => type, // given data in links, returns a node identifier string

    // Node (concept) styles
    nodeFontSize = '0.8em',     // the font-size of a nodes' text
    nodeFontColor = '0,0,0,255',    // default node text color as rgba
    nodeStroke = '0,0,0,255',   // node stroke (border) color
    nodeStrokeWidth = 1.5,      // node stroke (border) width in px
    nodeStrokeOpacity = 1,      // node stroke (border) opacity
    nodeRadius = 4,             // the (border) radius of the rectangles
    nodeRadiusFactor = 4,       // cmaps' border-radius is about 4 times bigger than d3s' radius.
    nodePaddingX = 10,          // the x-padding of a node so the text/title has some "space"
    nodePaddingY = 8,           // the y-padding of a node so the text/title has some "space"
    nodeBgr = '78,121,167,255', // default node background as rgba
    
    // Link styles
    linkStroke = "#999",          // link stroke color
    linkStrokeOpacity = 0.6,      // link stroke opacity
    linkStrokeWidth = 1.5,        // given data in links, returns a stroke width in pixels
    linkStrokeLinecap = "round",  // link stroke linecap

    // Marker (arrow)
    markerWidth = 10,
    markerHeight = 10,
    markerRefX = 10,
    markerRefY = 0,

    // Label styles (text on lines)
    labelFontSize = '0.7em',
    labelColor = '#555',

    // Render speed               // On initialization, render the graph faster (https://stackoverflow.com/a/26189110/5149302))
    ticksPerInitialRender = 15,   // Render graph once for every n ticks, or n'x speed. The higher, the faster the initialization
    ticksPerLaterRender = 2,      // Render graph once for every n ticks, or n'x speed. The lower, the better for dragging

    // Node (concepts)
    nodeStrength = 10,            // A positive value causes nodes to attract each other (default: -30) (github.com/twosixlabs/d3-force-reuse#manyBodyReuse_strength)
    nodeTheta = 0.9,              // sets the the accuracy of the Barnes–Hut approximation criterion; default 0.9; (github.com/twosixlabs/d3-force-reuse#manyBodyReuse_strength)

    // speed of nodes             // See https://stackoverflow.com/q/52194044/5149302
    simulationAlphaDecay = 0.01,  // This controls how quickly the simulation cools; defaults to 0.0228; (github.com/d3/d3-force#simulation_alphaDecay)
    simulationVelocityDecay = .5, // defaults to 0.4; (github.com/d3/d3-force#simulation_velocityDecay)
    simulationAlpha = 0.1,        // It decreases over time as the simulation "cools down". When alpha reaches alphaMin, simulation stops (github.com/d3/d3-force#simulation_alpha)
    
    // Node collision
    bboxSpacing = 15,             // Each node has a rectangular bboxextra for collision; here we can add some in px
    nodeCollideStrength = 0.1,    // The force when two nodes collide in the range [0,1] while default is 1 (github.com/d3/d3-force#collide_strength)
    nodeCollideIterations = 1,    // number of iterations per application, default is 1 (github.com/d3/d3-force#collide_iterations)
    
    // Node links
    linkStrength,                 // the strength accessor for each link; default is a function (github.com/d3/d3-force#link_strength)
    linkDistance,                 // the distance accessor for each link; default is 30 (github.com/d3/d3-force#link_distance)
    linkIterations,               // sets the number of iterations per application; default is 1 (github.com/d3/d3-force#link_iterations)

  } = {}                          // empty object if options are not given (default)
) {

  // Compute values
  const N = d3.map(nodes, nodeId).map(intern);
  const LS = d3.map(links, linkSource).map(intern);
  const LT = d3.map(links, linkTarget).map(intern);
  const LTy = d3.map(links, linkType).map(intern);
  if (nodeLabel === undefined) nodeLabel = (_, i) => N[i];
  const NL = nodeLabel == null ? null : d3.map(nodes, nodeLabel);
  const G = nodeGroup == null ? null : d3.map(nodes, nodeGroup).map(intern);
  const W = typeof linkStrokeWidth !== "function" ? null : d3.map(links, linkStrokeWidth);
  const L = typeof linkStroke !== "function" ? null : d3.map(links, linkStroke);
  // Short and long comments of nodes and links
  const NSC = nodeShortComment ? d3.map(nodes, nodeShortComment).map(intern) : null;
  const NLC = nodeLongComment  ? d3.map(nodes, nodeLongComment).map(intern)  : null;
  const LSC = linkShortComment ? d3.map(links, linkShortComment).map(intern) : null;
  const LLC = linkLongComment  ? d3.map(links, linkLongComment).map(intern)  : null;

  // Styles
  const NSTYLE = d3.map(nodes, nodeStyles).map(intern);

  // Replace the input nodes and links with mutable objects for the simulation.
  nodes = d3.map(nodes, (_, i) => ({id: N[i], label: NL[i], shortcomment: NSC[i], longcomment: NLC[i]}));
  links = d3.map(links, (_, i) => ({source: LS[i], target: LT[i], type: LTy[i]}));

  // Compute default domains.
  if (G && nodeGroups === undefined) nodeGroups = d3.sort(G);

  // Construct the scales.
  const color = nodeGroup == null ? null : d3.scaleOrdinal(nodeGroups, colors);

  // Construct the forces.
  // const forceNode = d3.forceManyBody(); // old by Mike Bostock
  const forceNode = d3.forceManyBodyReuse() // new by Robert Gove
  if (nodeStrength !== undefined) forceNode.strength(nodeStrength);
  if (nodeTheta !== undefined) forceNode.theta(nodeTheta);
  
  const forceLink = d3.forceLink(links).id(({index: i}) => N[i]);
  if (linkStrength !== undefined) forceLink.strength(linkStrength);
  if (linkDistance !== undefined) forceLink.distance(linkDistance);
  if (linkIterations !== undefined) forceLink.iterations(linkIterations);

  // Ticks per render
  ticksPerRender = ticksPerInitialRender;

  svg
    .attr("width", width)
    .attr("height", height)
    .attr("viewBox", [-width / 2, -height / 2, width, height])
    .attr("font-family", "Tahoma, Verdana, Arial, sans-serif")
    .attr("style", "max-width: 100%; height: auto; height: intrinsic; border: 1px solid");

  // ------------------------------------------------
  const marker = svg.append('defs').append('marker')
    .attr("id",'arrowhead')
    .attr('viewBox',[0, -5, 10, 10])  // Defines a coordinate system 10 wide and 10 high starting on (0,-5)
    .attr('refX',markerRefX)          // x-offset of the marker from the target-node
    .attr('refY',markerRefY)          // y-offset of the marker from the target-node
    .attr('orient','auto')
    .attr('markerWidth', markerWidth)
    .attr('markerHeight', markerHeight)
    .attr('xoverflow','visible')
    .append('svg:path')
    .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
    .attr('fill', '#999')
    .style('stroke','none');
  // -----------------------------------------------------

  // Append a group <g> to the svg element and add the lines
  const link = svg.append("g")
    .attr('class', 'links')
    .attr("stroke", typeof linkStroke !== "function" ? linkStroke : null)
    .attr("stroke-opacity", linkStrokeOpacity)
    .attr("stroke-width", typeof linkStrokeWidth !== "function" ? linkStrokeWidth : null)
    .attr("stroke-linecap", linkStrokeLinecap)
    .selectAll("line")
    .data(links)
    .join("line")
    .attr('marker-end','url(#arrowhead)');

  //Set link stroke-width and stroke (unused)
  //if (W) link.attr("stroke-width", ({index: i}) => W[i]);
  //if (L) link.attr("stroke", ({index: i}) => L[i]);

  // Append a group <g> to the svg element for the labels
  const label = svg.append("g")
    .attr('class', 'labels')
    .selectAll("label")
    .data(links)
    .join("g")
    .attr('class', 'label noselect');

  // Append the label text
  label.append('text')
    .attr("fill", labelColor)
    .attr("stroke", "none")
    .attr("font-size", labelFontSize)
    .attr('text-anchor', 'middle')
    .text(d => d.type);
    
  // Append a rect as background for the label
  label.insert("rect", ":first-child") 
    .attr("x", (d, i) => -(getDimension(i, label, 'width'))/2)
    .attr("y", (d, i) => -(getDimension(i, label, 'height'))/2)
    .attr('height', (d, i) => getDimension(i, label, 'height'))
    .attr("width", (d, i) => getDimension(i, label, 'width'))
    .attr("fill", 'transparent');

  // Append a <title> to the label
  if (LSC) label.append("title").text((d, i) => LSC[i]);

  // Append a group <g> to the svg element
  // The group will contain the <text> and <rect> elements later
  const node = svg.append("g")
    .attr('class', 'nodes')
    .selectAll("g")
    .data(nodes)
    .join("g")
    .attr('class', 'node')
    .attr('id', (d,i) => N[i])
    .attr('data-label', (d,i) => NL[i].toLowerCase());
 
  // append rectangle-text to node <g>    
  node.append("text")
    .text((d,i) => NL[i])
    .attr("fill", (d,i) => `rgba(${NSTYLE[i]['font-color'] ?? nodeFontColor })`)
    .attr("stroke", "none")
    .attr("font-size", nodeFontSize)
    .attr("text-anchor", "middle")
    .attr("dominant-baseline", "central") 

  // append rectangle (rect) as first element to node <g> to keep the text above
  // function getDimension() is used to caluculate the pos. and dimensions of the rect
  node.insert("rect", ":first-child")    
    .attr("x", (d, i) => -(getDimension(i, node, 'width') + nodePaddingX)/2)
    .attr("y", (d, i) => -(getDimension(i, node, 'height') + nodePaddingY)/2)
    .attr("width", (d, i) => getDimension(i, node, 'width') + nodePaddingX)
    .attr('height', (d, i) => getDimension(i, node, 'height') + nodePaddingY)
    .attr("rx", (d,i) =>  NSTYLE[i]['border-shape-rrarc'] ? Math.ceil(NSTYLE[i]['border-shape-rrarc']/nodeRadiusFactor): nodeRadius )
    .attr("fill", (d,i) => `rgba(${NSTYLE[i]['background-color'] ?? nodeBgr })`)
    .attr("stroke", (d,i) => `rgba(${NSTYLE[i]['border-color'] ?? nodeStroke })`)
    .attr("stroke-width", (d,i) => NSTYLE[i]['border-thickness'] ?? nodeStrokeWidth)
    .attr("stroke-opacity", nodeStrokeOpacity)

  // If group array exists, set color for each node
  if (G) node.attr("fill", ({index: i}) => color(G[i]));

  // If nodes' short comment array, add title to each node
  if (NSC) node.append("title").text((d,i) => NSC[i]);


  // ------------------- Collision detection (nodes) -------------------
  // Old circular collision detection:
  // var collide = d3.forceCollide().radius(100).iterations(3)

  // New rectangular collision detection of nodes:
  // Source: https://github.com/emeeks/d3-bboxCollide
  var nodeCollide = d3.bboxCollide(function (d,i) {
    let thisNode = node.nodes()[i];
    let bbox = thisNode.getBBox();
    let w = Math.ceil(bbox.width/2)+bboxSpacing;
    let h = Math.ceil(bbox.height/2)+bboxSpacing;
    return [[-w,-h],[w,h]]
  })
  .strength(nodeCollideStrength)
  .iterations(nodeCollideIterations)

  // See https://github.com/d3/d3-force
  const simulation = d3.forceSimulation(nodes)
    .alphaDecay(simulationAlphaDecay)
    .velocityDecay(simulationVelocityDecay)
    .force("link", forceLink)
    .force("charge", forceNode)
    .force("center",  d3.forceCenter())
    .force("collide", nodeCollide)
    .on("tick", ticked)
    .on("end", simulationEnded);

  // Adds drag events to node
  node
    .on("click", clicked)
    .call(drag(simulation));

  // ------------------- collision detection (labels) -------------------
  // Old circular collision detection:
  // labelCollide = d3.forceCollide().radius(50).iterations(3);

  /*
  // New rectangular collision detection of labels:
  // Source: https://github.com/emeeks/d3-bboxCollide
  var labelCollide = d3.bboxCollide(function (d,i) {
    let thisNode = label.nodes()[i];
    let bbox = thisNode.getBBox();
    let w = Math.ceil(bbox.width/2)+2;
    let h = Math.ceil(bbox.height/2)+2;
    return [[-w,-h],[w,h]]
  })
  .strength(0.1)
  .iterations(1)
  const labelSimulation = d3.forceSimulation(links)
    .force('link', d3.forceLink().links(links))
    .force('charge', d3.forceManyBody())
    .force('center', d3.forceCenter())
    .force('collision', labelCollide)
  
  label.call(drag(labelSimulation));
  */

  // ------------------- Click events -------------------

  // After user clicks a node, foundConcepts contains all elements of its data-label
  var foundConcepts = [];
  var foundConceptsIndex = 0;

  // Navigation elements like buttons, descriptions...
  const navPrevious = document.getElementById('navigate-previous');
  const navNext = document.getElementById('navigate-next');
  const navOccurences = document.getElementById('graph-navigation-occurrences');

  function handleNextPreviousNavigation(step) {
    foundConceptsIndex = mod_floor(foundConceptsIndex+step, foundConcepts.length);
    let el = foundConcepts[foundConceptsIndex];
    scrollToElement(el);
    el.classList.add('waypoint');
    document.getElementById('graph-navigation-occurrences').innerHTML = (foundConceptsIndex+1)+'/'+foundConcepts.length;
  }

  navPrevious.addEventListener('click', (e) => {
    handleNextPreviousNavigation(-1);
  });

  navNext.addEventListener('click', (e) => {
    handleNextPreviousNavigation(+1);
  });

  // ------------------- Helper functions -------------------

  function intern(value) {
    return value !== null && typeof value === "object" ? value.valueOf() : value;
  }

  // On initialization, we want to render the graph fast
  // After initialization, we want a smooth dragging
  function simulationEnded() {
    ticksPerRender = ticksPerLaterRender;
  }

  function ticked() {

    // How to speed up the force layout animation in d3.js?
    // Code provided by jshanley (stackoverflow.com/a/26189110/5149302) and this thread: github.com/d3/d3/issues/1519
    requestAnimationFrame(function render() {
      for (var i = 0; i < ticksPerRender; i++) {
        simulation.tick();
      }

      // Transform nodes <g> element by calculating their x and y values
      // limitPos() ensures that nodes stay in canvas, also considering their height and width
      node.attr("transform", function(d) {
        let nodeWidth = (getDimensionById(d.id, 'width')/2)+4;
        let nodeHeight = (getDimensionById(d.id, 'height')/2)+4;
        d.x = limitPosition(width, nodeWidth, d.x);
        d.y = limitPosition(height, nodeHeight, d.y);
        return `translate(${d.x} ${d.y})`
      });

      // Calculate position of links
      // Initial calculation of links by M. Bostock; problem: arrow-markers were hidden behind the nodes
      // link.attr("x1", d => limitPosition(width, 20, d.source.x)).attr("y1", d => limitPosition(height, 20, d.source.y)).attr("x2", d => limitPosition(width, 20, d.target.x)).attr("y2", d => limitPosition(height, 20, d.target.y)).text( d => getDim(d, 'width'));
      // This new calculation was written by Peter Collingridge, oluckyman and Gerardo Furtado
      // See stackoverflow.com/a/50261758/5149302 & stackoverflow.com/a/66664737/5149302
      link
        .attr("x1", d => d.source.x)
        .attr("y1", d => d.source.y)
        .attr("x2", (d,i) => getIntersection(
          d.source.x - d.target.x,
          d.source.y - d.target.y,
          d.target.x,
          d.target.y,
          getDim(d, 'width')/2,   // nodeWidth/2,
          getDim(d, 'height')/2,  // nodeHeight/2
        )[0])
        .attr("y2", (d,i) => getIntersection(
          d.source.x - d.target.x,
          d.source.y - d.target.y,
          d.target.x,
          d.target.y,
          getDim(d, 'width')/2,   // nodeWidth/2
          getDim(d, 'height')/2,  // nodeHeight/2
        )[1]);

      // transform the labels, source: https://stackoverflow.com/a/36057066/5149302        
      label.attr("transform", function(d) {
        var angle = Math.atan((d.source.y - d.target.y) / (d.source.x - d.target.x)) * 180 / Math.PI;
        let x = limitPosition(width, 20, (d.source.x + d.target.x) / 2);
        let y = limitPosition(height, 20, (d.source.y + d.target.y) / 2);
        return `translate(${x} ${y})rotate(${angle})`
      });
      
      if (simulation.alpha > simulationAlpha) {
        requestAnimationFrame(render);
      }

    })

  }

  // Function ensures that a coordinate doesn't leave the viewport
  // Param limit (int): usually the max with or height of the canvas
  // Param offset (int): offset from canvas border to ensure that node is not overlapping
  // Param value (int): usually the x or y position of an element
  function limitPosition(limit, offset, value) {
    return Math.max(-limit/2+offset, Math.min(limit/2-offset, value))
  }

  // The function "drag" handles the drag events start, still dragging, and end
  // Initially, there was the problem that a 'click' also started the drag event.
  // We solved this by moving 'simulation.alphaTarget(0.3).restart(); from dragstarted 
  // to 'dragging' wrapped in an if clause, ensuring that simulation is restarted once.
  var dragStarted = false;
  function drag(simulation) {   
    function dragstarted(event) {
      if (!event.active) dragStarted = true;
      event.subject.fx = event.subject.x;
      event.subject.fy = event.subject.y;
    }    
    function dragging(event) {
      if (dragStarted) {
        simulation.alphaTarget(0.3).restart();
        dragStarted = false;
      }
      event.subject.fx = limitPosition(width, 20, event.x);
      event.subject.fy = limitPosition(height, 20, event.y);
    }    
    function dragended(event) {
      if (!event.active) simulation.alphaTarget(0);
      event.subject.fx = null;
      event.subject.fy = null;
    }    
    return d3.drag()
      .on("start", dragstarted)
      .on("drag", dragging)
      .on("end", dragended);
  }

  function clicked(e, obj) {

    // node was dragged. not clicked
    if (event.defaultPrevented) return;
        
    // Reset index of navigation (next, previous)
    foundConceptsIndex = 0;
    
    // In the portfolio, find all elements by clicked data-label
    let label = obj.label.toLowerCase().replaceAll(' ', '_');
    let mainContainer = document.getElementById(mainContentId);
    foundConcepts = mainContainer.querySelectorAll(`[data-label="${label}"]`);
        
    // Set info below the graph like label, short comment, long comment, Label frequency
    document.getElementById('graph-navigation-label').innerHTML = obj.label;
    document.getElementById('graph-navigation-shortcomment').innerHTML = obj.shortcomment;
    document.getElementById('graph-navigation-longcomment').innerHTML = obj.longcomment;
    
    let navigation = document.getElementById('navigate-portfolio');

    if (foundConcepts.length > 0) {
      
      // Get element and style it
      let el = foundConcepts[foundConceptsIndex];
      el.classList.add('waypoint');
      
      scrollToElement(el);
      
      // Set concept counter
      navOccurences.innerHTML = (foundConceptsIndex+1)+'/'+foundConcepts.length;
      // Enable node navigation buttons
      navPrevious.removeAttribute("disabled");
      navNext.removeAttribute("disabled");
    } else {
      navOccurences.innerHTML = 0;
      // Disable node navigation buttons
      navPrevious.setAttribute('disabled', '');
      navNext.setAttribute('disabled', '');
    }    
  }

  // Function calculates the intersection between lines and rectangles.
  // It helps to position the lines on the rectangle border instead in its core.
  // This function was written by Peter Collingridge, oluckyman and Gerardo Furtado
  // See stackoverflow.com/a/50261758/5149302 & stackoverflow.com/a/66664737/5149302
  function getIntersection(dx, dy, cx, cy, w, h) {
    // Hit vertical edge of box1
    if (Math.abs(dy / dx) < h / w) {
      return [cx + (dx > 0 ? w : -w), cy + dy * w / Math.abs(dx)];
    } else {
      // Hit horizontal edge of box1
      return [cx + dx * h / Math.abs(dy), cy + (dy > 0 ? h : -h)];
    }
  };

  // Helper function returning a dimension (e.g. width) of a nodes' bounding box
  // When using .append, we have access to the data (d) and the iterator (i)
  // We use the iterator to get the current node from the node array (n)
  function getDimension(i, n, dim) {
    let thisNode = n.nodes()[i];
    if (thisNode) return thisNode.getBBox()[dim];
  }

  function getDimensionById(id,dim) {
    let node = d3.select(`[id="${id}"]`).node();
    return node.getBBox()[dim];
  }

  // TODO: replace by getNodeById and getDimension
  function getDim(d, dim) {
    let id = d.target.id;
    //id = id.replaceAll(/\W/g,'_');     // TODO replace by valid id from dataset
    let node = d3.select(`[id="${id}"]`)
    let w = getDimension(0, node, dim);
    return w
  }

  function getNodeById(d) {
    let id = d.target.id;
    //id = id.replaceAll(/\W/g,'_');     // TODO replace by valid id from dataset
    return d3.select(`[id="${id}"]`);
  }

  return Object.assign(svg.node(), {scales: {color}});
}

// ------------------------------------------------------

// Disable waypoint scroll listener and scroll to element
function scrollToElement(el, disableListener = true) {
  if (disableListener) {
    if (Waypoint) {Waypoint.disableAll();}
    nodeClicked = true;
  }
  el.scrollIntoView({behavior: 'smooth', block: 'center'});
}

// Function allows circular array by returning correct index
// Credits: Joseph Thomson on stackoverflow.com/a/43827557/5149302
function mod_floor(a, n) {
    return ((a % n) + n) % n;
}

// TODO: this is a bad workourd. 
// Solve by moving everything to a class
// Param l (string): the attribute value
// Param a (string): the attribute key (e.g. "label", resulting data-label)
function getNodesByDataAttr(value, attr='label') {
  var svg = d3.select(graphId);
  var labels = svg.select(`[data-${attr}="${value}"]`);
  return labels;
}

const graphId = "#portfolio-graph"; // TODO: inti differently
var nodeClicked = false;

// D3: Read .json file and build the graph
d3
  .json("data/Programmierung-v7-221020.json")
  .then((data) => {

    // In this example, the graph-height depends on viewport
    let vh = document.documentElement.clientHeight;
    let height = vh*0.75;

    // Some nodes do not own or only bring a small set of styles.
    // Instead, they own a stylesheet-id pointing to a stylesheet object
    // Here, we look up the styles in the sheet and add them to the corresp. node
    data.nodes = data.nodes.map((n,i) => {
      let styles = data.stylesheet.find(s => s.id == n.styles['stylesheet-id'])
      if (styles) { n.styles = Object.assign(n.styles, styles)}
      return n;
    });

    var graph = ForceGraph( 
      data.nodes,
      data.links,
      {
        svg: d3.select(graphId),
        mainContentId: 'main',
        nodeId: data => data.id,
        nodeLabel: data => data.label,
        nodeShortComment: data => data['short-comment'],
        nodeLongComment: data => data['long-comment'],
        nodeStyles: data => data.styles,
        //nodeGroup: data => data.group,
        linkType: data => data.label,
        linkStrokeWidth: l => Math.sqrt(l.value),
        width: 1000,
        height: height,
      }
    );
  });
