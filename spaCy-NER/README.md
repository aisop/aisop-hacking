# doing NER with spaCy

Goal: Enrich an HTML file with the recognition results of entities that we can identify within our knowledge maps.

## Installation

as everything spacy

`pip install spacy`
`python -m spacy download de_core_news_sm`
`python -m spacy download de_core_news_lg`

## Straight out of the template

From the [docu](https://spacy.io/models/de#de_core_news_sm): 
simply run `attempt1.py`.

Or... run  [the code of students](https://github.com/jataf/keywordextraction/blob/main/keyword_extraction.py) having made sure that the port 5000 is free before which displays the analysis results using [displacy](https://spacy.io/api/top-level#displacy).